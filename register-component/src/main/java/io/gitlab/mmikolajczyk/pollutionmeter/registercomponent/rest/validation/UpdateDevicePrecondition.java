package io.gitlab.mmikolajczyk.pollutionmeter.registercomponent.rest.validation;

import io.gitlab.mmikolajczyk.pollutionmeter.common.api.error.APIErrorException;
import io.gitlab.mmikolajczyk.pollutionmeter.common.api.error.ErrorCodes;
import io.gitlab.mmikolajczyk.pollutionmeter.registercomponent.entity.DeviceEntity;
import io.gitlab.mmikolajczyk.pollutionmeter.registercomponent.repository.DeviceRepository;
import io.gitlab.mmikolajczyk.pollutionmeter.registercomponent.rest.body.UpdateDeviceDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class UpdateDevicePrecondition implements Precondition<UpdateDeviceDto> {

    private final DeviceRepository deviceRepository;

    @Autowired
    public UpdateDevicePrecondition(DeviceRepository deviceRepository) {
        this.deviceRepository = deviceRepository;
    }

    @Override
    public void check(UpdateDeviceDto objToCheck) {
        Optional<DeviceEntity> existingDevice = deviceRepository.findByIdLock(objToCheck.getDeviceId());
        checkExisting(existingDevice);
    }

    private void checkExisting(Optional<DeviceEntity> entity) {
        if (entity.isEmpty()) {
            throw new APIErrorException(ErrorCodes.DEVICE_NOT_EXISTS);
        }
    }
}
