package io.gitlab.mmikolajczyk.pollutionmeter.registercomponent.util;

public final class DateUtils {

    private DateUtils() {
    }

    public static final String FORMAT = "yyyy-MM-dd HH:mm:ss";

}
