package io.gitlab.mmikolajczyk.pollutionmeter.registercomponent.rest.validation;

import lombok.experimental.UtilityClass;

@UtilityClass
public class Constants {
    public final String MAC_ADDRESS_PATTERN = "^([a-fA-F0-9]{2}[:-]){5}([a-fA-F0-9]{2})$";
}
