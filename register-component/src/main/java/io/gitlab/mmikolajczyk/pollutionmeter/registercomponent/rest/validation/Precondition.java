package io.gitlab.mmikolajczyk.pollutionmeter.registercomponent.rest.validation;

public interface Precondition<T> {

    void check(T objToCheck);
}
