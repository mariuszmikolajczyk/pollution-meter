package io.gitlab.mmikolajczyk.pollutionmeter.registercomponent.rest.validation;

import io.gitlab.mmikolajczyk.pollutionmeter.common.api.error.APIErrorException;
import io.gitlab.mmikolajczyk.pollutionmeter.common.api.error.ErrorCodes;
import io.gitlab.mmikolajczyk.pollutionmeter.registercomponent.repository.DeviceRepository;
import io.gitlab.mmikolajczyk.pollutionmeter.registercomponent.rest.body.CreateDeviceRequestBody;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class NewDevicePrecondition implements Precondition<CreateDeviceRequestBody> {

    private final DeviceRepository deviceRepository;

    @Autowired
    public NewDevicePrecondition(DeviceRepository deviceRepository) {
        this.deviceRepository = deviceRepository;
    }

    @Override
    public void check(CreateDeviceRequestBody deviceReqBody) {
        deviceRepository.findByMacAddress(deviceReqBody.getMacAddress()).ifPresent((d) -> {
            throw new APIErrorException(ErrorCodes.MAC_ADDRESS_DUPLICATE);
        });
    }
}
