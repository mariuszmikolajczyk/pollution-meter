package io.gitlab.mmikolajczyk.pollutionmeter.registercomponent.util;

import io.gitlab.mmikolajczyk.pollutionmeter.registercomponent.entity.DeviceEntity;
import io.gitlab.mmikolajczyk.pollutionmeter.registercomponent.rest.body.ResponseDeviceDto;
import io.gitlab.mmikolajczyk.pollutionmeter.registercomponent.rest.body.UpdateDeviceDto;
import org.springframework.beans.BeanUtils;

import java.time.format.DateTimeFormatter;

public class ObjectMapper {
    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DateUtils.FORMAT);

    public static DeviceEntity mapToDevice(Object object) {
        DeviceEntity device = new DeviceEntity();
        BeanUtils.copyProperties(object, device);
        return device;
    }

    public static DeviceEntity mapToDevice(UpdateDeviceDto dto) {
        DeviceEntity entity = mapToDevice(dto.getReqBody());
        entity.setDeviceId(dto.getDeviceId());
        return entity;
    }

    public static ResponseDeviceDto mapToResDevice(DeviceEntity entity) {
        var responseDto = new ResponseDeviceDto();
        BeanUtils.copyProperties(entity, responseDto);
        responseDto.setCreatedDate(entity.getCreatedDate().format(formatter));
        return responseDto;
    }
}
