package io.gitlab.mmikolajczyk.pollutionmeter.registercomponent.rest.body;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class UpdateDeviceDto {
    private Long deviceId;
    private UpdateDeviceRequestBody reqBody;
}
