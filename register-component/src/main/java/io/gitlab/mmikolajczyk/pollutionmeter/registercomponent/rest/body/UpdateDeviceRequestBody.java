package io.gitlab.mmikolajczyk.pollutionmeter.registercomponent.rest.body;

import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Data
public class UpdateDeviceRequestBody {
    @NotNull(message = "cannot be null"/*, groups = First.class*/)
    @Min(value = -180, message = "has incorrect value")
    @Max(value = 180, message = "has incorrect value")
    private Float longitude;
    @NotNull(message = "cannot be null")
    @Min(value = -90, message = "has incorrect value")
    @Max(value = 90, message = "has incorrect value")
    private Float latitude;
    private String name;
    private String locationName;
    private String description;
}
