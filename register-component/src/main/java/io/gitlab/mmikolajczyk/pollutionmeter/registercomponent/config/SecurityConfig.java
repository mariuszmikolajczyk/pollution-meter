package io.gitlab.mmikolajczyk.pollutionmeter.registercomponent.config;

import io.gitlab.mmikolajczyk.pollutionmeter.common.security.jwt.JwtAuthoritiesClaimToGrantedAuthoritiesConverter;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.converter.Converter;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationConverter;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Override
    public void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers(HttpMethod.GET, "/device/**").hasRole("DATA")
                .antMatchers("/device/**").hasRole("REGISTRATION")
                .and()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .csrf().disable()
                .oauth2ResourceServer(r -> r.jwt()
                        .jwtAuthenticationConverter(createCustomConverter())
                );
    }

    private Converter<Jwt, ? extends AbstractAuthenticationToken> createCustomConverter() {
        var defaultConverter = new JwtAuthenticationConverter();
        defaultConverter.setJwtGrantedAuthoritiesConverter(new JwtAuthoritiesClaimToGrantedAuthoritiesConverter());
        return defaultConverter;
    }
}
