package io.gitlab.mmikolajczyk.pollutionmeter.registercomponent;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan({"io.gitlab.mmikolajczyk.pollutionmeter.registercomponent", "io.gitlab.mmikolajczyk.pollutionmeter.common"})
public class RegisterComponentApplication {

    public static void main(String[] args) {
        SpringApplication.run(RegisterComponentApplication.class, args);
    }
}

