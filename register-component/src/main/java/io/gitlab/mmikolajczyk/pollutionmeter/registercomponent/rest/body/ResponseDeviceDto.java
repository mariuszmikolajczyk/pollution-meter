package io.gitlab.mmikolajczyk.pollutionmeter.registercomponent.rest.body;

import lombok.Data;

@Data
public class ResponseDeviceDto {
    private Long deviceId;
    private Float longitude;
    private Float latitude;
    private String locationName;
    private String name;
    private String description;
    private String createdDate;
    private String macAddress;
}
