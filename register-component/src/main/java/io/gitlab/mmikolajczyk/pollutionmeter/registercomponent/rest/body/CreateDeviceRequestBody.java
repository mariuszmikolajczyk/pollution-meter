package io.gitlab.mmikolajczyk.pollutionmeter.registercomponent.rest.body;

import io.gitlab.mmikolajczyk.pollutionmeter.common.api.error.ErrorCodes;
import io.gitlab.mmikolajczyk.pollutionmeter.registercomponent.rest.validation.Constants;
import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Data
public class CreateDeviceRequestBody {
    @NotNull(message = ErrorCodes.MessageSuffix.NULL_VALUE)
    @Min(value = -180, message = ErrorCodes.MessageSuffix.INCORRECT_VALUE)
    @Max(value = 180, message = ErrorCodes.MessageSuffix.INCORRECT_VALUE)
    private Float longitude;
    @NotNull(message = ErrorCodes.MessageSuffix.NULL_VALUE)
    @Min(value = -90, message = ErrorCodes.MessageSuffix.INCORRECT_VALUE)
    @Max(value = 90, message = ErrorCodes.MessageSuffix.INCORRECT_VALUE)
    private Float latitude;
    private String name;
    private String locationName;
    private String description;
    @NotNull(message = ErrorCodes.MessageSuffix.NULL_VALUE)
    @Pattern(regexp = Constants.MAC_ADDRESS_PATTERN, message = ErrorCodes.MessageSuffix.INCORRECT_VALUE)
    private String macAddress;
}
