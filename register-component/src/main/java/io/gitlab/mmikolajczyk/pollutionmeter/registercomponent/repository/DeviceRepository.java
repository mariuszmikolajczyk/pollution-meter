package io.gitlab.mmikolajczyk.pollutionmeter.registercomponent.repository;

import io.gitlab.mmikolajczyk.pollutionmeter.registercomponent.entity.DeviceEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.persistence.LockModeType;
import java.util.Optional;

@Repository
public interface DeviceRepository extends JpaRepository<DeviceEntity, Long> {

    @Lock(value = LockModeType.PESSIMISTIC_WRITE)
    @Query("select d from DeviceEntity d where d.deviceId=:id")
    Optional<DeviceEntity> findByIdLock(@Param("id") Long id);

    Optional<DeviceEntity> findByMacAddress(String macAddress);

    @Query("update DeviceEntity d set d.longitude=:longitude, d.latitude=:latitude, d.name=:name, d.description=:description, d.locationName=:locationName where d.deviceId=:deviceId")
    @Modifying(clearAutomatically = true)
    void simplyUpdateDevice(@Param("longitude") float longitude, @Param("latitude") float latitude, @Param("name") String name, @Param("description") String description, @Param("locationName") String locationName, @Param("deviceId") Long deviceId);
}
