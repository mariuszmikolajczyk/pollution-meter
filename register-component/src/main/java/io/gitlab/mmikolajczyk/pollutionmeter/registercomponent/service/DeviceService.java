package io.gitlab.mmikolajczyk.pollutionmeter.registercomponent.service;

import io.gitlab.mmikolajczyk.pollutionmeter.common.api.error.APIErrorException;
import io.gitlab.mmikolajczyk.pollutionmeter.common.api.error.ErrorCodes;
import io.gitlab.mmikolajczyk.pollutionmeter.registercomponent.entity.DeviceEntity;
import io.gitlab.mmikolajczyk.pollutionmeter.registercomponent.repository.DeviceRepository;
import io.gitlab.mmikolajczyk.pollutionmeter.registercomponent.rest.body.CreateDeviceRequestBody;
import io.gitlab.mmikolajczyk.pollutionmeter.registercomponent.rest.body.ResponseDeviceDto;
import io.gitlab.mmikolajczyk.pollutionmeter.registercomponent.rest.body.UpdateDeviceDto;
import io.gitlab.mmikolajczyk.pollutionmeter.registercomponent.rest.validation.NewDevicePrecondition;
import io.gitlab.mmikolajczyk.pollutionmeter.registercomponent.rest.validation.UpdateDevicePrecondition;
import io.gitlab.mmikolajczyk.pollutionmeter.registercomponent.util.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

import static java.util.stream.Collectors.toList;

@Service
public class DeviceService {

    private DeviceRepository repository;
    private NewDevicePrecondition newDevicePrecondition;
    private final UpdateDevicePrecondition updateDevicePrecondition;

    @Autowired
    public DeviceService(DeviceRepository repository, NewDevicePrecondition newDevicePrecondition, UpdateDevicePrecondition updateDevicePrecondition) {
        this.repository = repository;
        this.newDevicePrecondition = newDevicePrecondition;
        this.updateDevicePrecondition = updateDevicePrecondition;
    }

    public ResponseDeviceDto registerNewDevice(CreateDeviceRequestBody device) {
        newDevicePrecondition.check(device);
        DeviceEntity deviceEntity = ObjectMapper.mapToDevice(device);
        return ObjectMapper.mapToResDevice(repository.save(deviceEntity));
    }

    public List<ResponseDeviceDto> getAll() {
        return repository.findAll().stream().map(ObjectMapper::mapToResDevice).collect(toList());
    }

    @Transactional //to make @Lock works it has to be in tx
    public ResponseDeviceDto getById(Long id) {
        return ObjectMapper.mapToResDevice(repository.findByIdLock(id).orElseThrow(() -> new APIErrorException(ErrorCodes.DEVICE_NOT_EXISTS)));
    }

    @Transactional(isolation = Isolation.READ_COMMITTED)
    public void removeDevice(Long id) {
        Optional<DeviceEntity> device = repository.findByIdLock(id);
        if (device.isPresent()) {
            repository.delete(device.get());
        } else {
            throw new APIErrorException(ErrorCodes.DEVICE_NOT_EXISTS);
        }
    }

    @Transactional(isolation = Isolation.READ_COMMITTED)
    public ResponseEntity processDeviceUpdate(UpdateDeviceDto deviceUpdateDto) {
        updateDevicePrecondition.check(deviceUpdateDto);
        var updatedEntity = updateDevice(deviceUpdateDto);
        return ResponseEntity.ok(updatedEntity);
    }

    private ResponseDeviceDto updateDevice(UpdateDeviceDto dto) {
        repository.simplyUpdateDevice(dto.getReqBody().getLongitude(), dto.getReqBody().getLatitude(), dto.getReqBody().getName(), dto.getReqBody().getDescription(), dto.getReqBody().getLocationName(), dto.getDeviceId());
        return ObjectMapper.mapToResDevice(repository.findById(dto.getDeviceId()).get());
    }

    public ResponseDeviceDto getByMacAddress(String macAddress) {
        return ObjectMapper.mapToResDevice(repository.findByMacAddress(macAddress).orElseThrow(() -> new APIErrorException(ErrorCodes.DEVICE_MAC_ADDRESS_NOT_EXISTS)));
    }
}
