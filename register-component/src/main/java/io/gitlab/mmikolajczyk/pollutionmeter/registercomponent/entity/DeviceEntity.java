package io.gitlab.mmikolajczyk.pollutionmeter.registercomponent.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.gitlab.mmikolajczyk.pollutionmeter.registercomponent.util.DateUtils;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Objects;

@Table(name = "device")
@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class DeviceEntity {
    @Id
    @Column(name = "device_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long deviceId;
    @Column
    private float longitude;
    @Column
    private float latitude;
    @Column
    private String description;
    @Column(name = "location_name")
    private String locationName;
    @Column(name = "mac_address")
    private String macAddress;
    @Column
    private String name;
    @Column(name = "created_date")
    @JsonFormat(pattern = DateUtils.FORMAT)
    private LocalDateTime createdDate = LocalDateTime.now();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DeviceEntity that = (DeviceEntity) o;

        if (!Objects.equals(deviceId, that.deviceId)) return false;
        if (Float.compare(that.longitude, longitude) != 0) return false;
        return Float.compare(that.latitude, latitude) == 0;
    }

    @Override
    public int hashCode() {
        int result = deviceId.intValue();
        result = 31 * result + (longitude != +0.0f ? Float.floatToIntBits(longitude) : 0);
        result = 31 * result + (latitude != +0.0f ? Float.floatToIntBits(latitude) : 0);
        return result;
    }
}
