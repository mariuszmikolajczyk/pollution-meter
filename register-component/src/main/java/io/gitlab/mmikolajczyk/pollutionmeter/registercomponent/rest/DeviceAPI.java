package io.gitlab.mmikolajczyk.pollutionmeter.registercomponent.rest;

import io.gitlab.mmikolajczyk.pollutionmeter.common.api.error.APIErrorException;
import io.gitlab.mmikolajczyk.pollutionmeter.common.api.error.ErrorCodes;
import io.gitlab.mmikolajczyk.pollutionmeter.registercomponent.rest.body.CreateDeviceRequestBody;
import io.gitlab.mmikolajczyk.pollutionmeter.registercomponent.rest.body.ResponseDeviceDto;
import io.gitlab.mmikolajczyk.pollutionmeter.registercomponent.rest.body.UpdateDeviceDto;
import io.gitlab.mmikolajczyk.pollutionmeter.registercomponent.rest.body.UpdateDeviceRequestBody;
import io.gitlab.mmikolajczyk.pollutionmeter.registercomponent.rest.validation.Constants;
import io.gitlab.mmikolajczyk.pollutionmeter.registercomponent.service.DeviceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Pattern;
import java.util.List;

@RestController
@RequestMapping("/device")
public class DeviceAPI {

    private DeviceService deviceService;

    @Autowired
    public DeviceAPI(DeviceService deviceService) {
        this.deviceService = deviceService;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseDeviceDto registerDevice(@Valid @RequestBody CreateDeviceRequestBody deviceReqBody) {
        return deviceService.registerNewDevice(deviceReqBody);
    }

    @GetMapping
    public List<ResponseDeviceDto> getAllDevice() throws APIErrorException {
        return deviceService.getAll();
    }

    @GetMapping("/{id}")
    public ResponseDeviceDto getDevice(@PathVariable Long id) {
        return deviceService.getById(id);
    }

    @PutMapping("/{id}")
    public ResponseEntity updateDevice(@PathVariable long id, @RequestBody @Valid UpdateDeviceRequestBody body) {
        return deviceService.processDeviceUpdate(new UpdateDeviceDto(id, body));
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteDevice(@PathVariable Long id) {
        deviceService.removeDevice(id);
    }

    @GetMapping(path = "/search", params = "macAddress")
    public ResponseDeviceDto findByMACAddress(@RequestParam @Valid @Pattern(regexp = Constants.MAC_ADDRESS_PATTERN, message = ErrorCodes.MessageSuffix.INCORRECT_VALUE) String macAddress) {
        return deviceService.getByMacAddress(macAddress);
    }
}
