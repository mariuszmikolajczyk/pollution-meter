package io.gitlab.mmikolajczyk.pollutionmeter.registercomponent;

import io.gitlab.mmikolajczyk.pollutionmeter.common.api.error.ErrorCodes;
import io.gitlab.mmikolajczyk.pollutionmeter.registercomponent.entity.DeviceEntity;
import io.gitlab.mmikolajczyk.pollutionmeter.registercomponent.repository.DeviceRepository;
import io.gitlab.mmikolajczyk.pollutionmeter.registercomponent.rest.body.CreateDeviceRequestBody;
import io.gitlab.mmikolajczyk.pollutionmeter.registercomponent.rest.body.ResponseDeviceDto;
import io.gitlab.mmikolajczyk.pollutionmeter.registercomponent.util.ObjectMapper;
import io.gitlab.mmikolajczyk.pollutionmeter.registercomponent.util.TestUtils;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.RequestPostProcessor;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import javax.annotation.PostConstruct;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.stream.IntStream;

import static java.util.stream.Collectors.toList;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest(properties = {"eureka.client.enabled=false"})
@Transactional(rollbackFor = Throwable.class)
@WebAppConfiguration
@Slf4j
//won't work because of deadlock - to check expression Spring Context mus be initialized - to initialize Spring Context there is required Database connections, which is unavailable (this is the reason of skipping tests)
//@EnabledIf(expression = "${spring.profiles.active=='test' || spring.profiles.active=='ci'}", loadContext = true)
public class RegisterComponentApplicationTests {
    private static final String TESTED_ENDPOINT = "/device";
    private static Random random = new Random(9933);

    private MockMvc mockMvc;
    @Autowired
    private DeviceRepository deviceRepository;

    private CreateDeviceRequestBody preparedDeviceBody = new CreateDeviceRequestBody();

    @Autowired
    private WebApplicationContext context;

    private static DeviceEntity[] initDevices = new DeviceEntity[5];
    private RequestPostProcessor dataUser = user("tester").roles("DATA");
    private RequestPostProcessor registrationUser = user("tester").roles("REGISTRATION");

    @PostConstruct
    public void init() {
        mockMvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();
    }

    @BeforeAll
    private static void addInitData(@Autowired DeviceRepository deviceRepository) {
        final int n = 5;
        IntStream.range(0, n).forEach((index) -> {
            var testDevice = createTestDevice();
            var savedEntity = deviceRepository.saveAndFlush(testDevice);
            initDevices[index] = savedEntity;
        });
    }

    private static DeviceEntity createTestDevice() {
        return new DeviceEntity(null, random.nextFloat() + 20f, random.nextFloat() + 10f, null, "no-data", "00-ff-00-ff-00-" + random.nextInt(99), "test-device", LocalDateTime.now());
    }

    @BeforeEach
    private void prepareTestDevice() {
        preparedDeviceBody.setLatitude(-33.0f);
        preparedDeviceBody.setLongitude(15.0f);
        preparedDeviceBody.setMacAddress("00-ff-00-ff-00-17");
    }

    @Test
    public void getDevices() throws Exception {
        List<DeviceEntity> devices = deviceRepository.findAll();

        MvcResult result = mockMvc.perform(get(TESTED_ENDPOINT).with(dataUser))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(devices.size()))).andReturn();

        String resultJSONString = result.getResponse().getContentAsString();
        List<DeviceEntity> devicesFromResponse = TestUtils.mapJsonStringToList(resultJSONString, ResponseDeviceDto.class).stream().map(ObjectMapper::mapToDevice).collect(toList());

        assertEquals(devices, devicesFromResponse);
    }

    @Test
    public void registerDevice() throws Exception {
        final String description = "Some description";
        preparedDeviceBody.setDescription("registerDevice()");
        preparedDeviceBody.setName(description);
        preparedDeviceBody.setLocationName("Somewhere on Earth");

        var result = mockMvc.perform(post(TESTED_ENDPOINT).with(registrationUser)
                .contentType(MediaType.APPLICATION_JSON)
                .content(TestUtils.mapObjectToJsonString(preparedDeviceBody)))
                .andExpect(status().isCreated())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.device_id", greaterThan(0)))
                .andExpect(jsonPath("$.longitude", notNullValue()))
                .andExpect(jsonPath("$.latitude", notNullValue()))
                .andExpect(jsonPath("$.name", not(emptyString())))
                .andExpect(jsonPath("$.description", not(emptyString())))
                .andExpect(jsonPath("$.created_date", not(emptyString())))
                .andExpect(jsonPath("$.location_name", not(emptyString())))
                .andReturn();

        var entityFromCreateBody = ObjectMapper.mapToDevice(preparedDeviceBody);
        DeviceEntity entityFromResponse = TestUtils.mapJsonToObject(result.getResponse().getContentAsString(), DeviceEntity.class);
        assertNotNull(entityFromResponse.getCreatedDate());
        //to ignore in equality test
        entityFromCreateBody.setCreatedDate(null);
        entityFromResponse.setCreatedDate(null);
        entityFromResponse.setDeviceId(null);
        assertEquals(entityFromCreateBody, entityFromResponse);
    }

    @Test
    void nullLongitude() throws Exception {
        preparedDeviceBody.setLongitude(null);

        mockMvc.perform(post(TESTED_ENDPOINT).with(registrationUser)
                .contentType(MediaType.APPLICATION_JSON)
                .content(TestUtils.mapObjectToJsonString(preparedDeviceBody)))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("message", is("Invalid input!")))
                .andExpect(jsonPath("timestamp", notNullValue()))
                .andExpect(jsonPath("details", is("Field 'longitude' cannot be null")));
    }

    @Test
    void nullLatitude() throws Exception {
        preparedDeviceBody.setLatitude(null);

        mockMvc.perform(post(TESTED_ENDPOINT).with(registrationUser)
                .contentType(MediaType.APPLICATION_JSON)
                .content(TestUtils.mapObjectToJsonString(preparedDeviceBody)))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("message", is("Invalid input!")))
                .andExpect(jsonPath("timestamp", notNullValue()))
                .andExpect(jsonPath("details", is("Field 'latitude' cannot be null")));
    }

    @Test
    void locationParamNull() throws Exception {
        preparedDeviceBody.setLatitude(null);
        preparedDeviceBody.setLongitude(null);

        mockMvc.perform(post(TESTED_ENDPOINT).with(registrationUser)
                .contentType(MediaType.APPLICATION_JSON)
                .content(TestUtils.mapObjectToJsonString(preparedDeviceBody)))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("message", is("Invalid input!")))
                .andExpect(jsonPath("timestamp", notNullValue()))
                .andExpect(jsonPath("details", is(oneOf("Field 'latitude' cannot be null", "Field 'longitude' cannot be null"))));
    }

    @Test
    void exceededLongitudeValue() throws Exception {
        preparedDeviceBody.setLongitude(190f);

        mockMvc.perform(post(TESTED_ENDPOINT).with(registrationUser)
                .contentType(MediaType.APPLICATION_JSON)
                .content(TestUtils.mapObjectToJsonString(preparedDeviceBody)))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("message", is("Invalid input!")))
                .andExpect(jsonPath("timestamp", notNullValue()))
                .andExpect(jsonPath("details", is("Field 'longitude' has incorrect value")));
    }

    @Test
    void exceededLatitudeValue() throws Exception {
        preparedDeviceBody.setLatitude(100f);

        mockMvc.perform(post(TESTED_ENDPOINT).with(registrationUser)
                .contentType(MediaType.APPLICATION_JSON)
                .content(TestUtils.mapObjectToJsonString(preparedDeviceBody)))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("message", is("Invalid input!")))
                .andExpect(jsonPath("timestamp", notNullValue()))
                .andExpect(jsonPath("details", is("Field 'latitude' has incorrect value")));
    }

    @Test
    void getByIdSuccessful() throws Exception {
        var result = mockMvc.perform(get(TESTED_ENDPOINT + "/" + initDevices[0].getDeviceId()).with(dataUser)).andReturn();
        DeviceEntity deviceFromResponse = ObjectMapper.mapToDevice(TestUtils.mapJsonToObject(result.getResponse().getContentAsString(), ResponseDeviceDto.class));
        assertEquals(initDevices[0], deviceFromResponse);
    }

    @Test
    void getByIdNotExistingId() throws Exception {
        mockMvc.perform(get(TESTED_ENDPOINT + "/" + initDevices[initDevices.length - 1].getDeviceId() + 1L).with(dataUser))
                .andExpect(status().is(ErrorCodes.DEVICE_NOT_EXISTS.getHttpStatus().value()))
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.message", is("Invalid input!")))
                .andExpect(jsonPath("$.details", is(ErrorCodes.DEVICE_NOT_EXISTS.getMessage())));
    }

    @Test
    void getByIdLiteralId() throws Exception {
        mockMvc.perform(get(TESTED_ENDPOINT + "/abcde").with(dataUser))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.message", is("Invalid input!")))
                .andExpect(jsonPath("$.details", is(ErrorCodes.Message.UNABLE_TO_PARSE_PATH_PARAM.toString())));
    }

    @Test
    void updateDeviceSuccess() throws Exception {
        preparedDeviceBody.setDescription("updateDeviceSuccess");
        MvcResult result = mockMvc.perform(post(TESTED_ENDPOINT).with(registrationUser)
                .contentType(MediaType.APPLICATION_JSON)
                .content(TestUtils.mapObjectToJsonString(preparedDeviceBody)))
                .andExpect(status().isCreated())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.device_id", greaterThan(0))).andReturn();

        DeviceEntity entityFromResponse = ObjectMapper.mapToDevice(TestUtils.mapJsonToObject(result.getResponse().getContentAsString(), ResponseDeviceDto.class));

        preparedDeviceBody.setLongitude(40f);

        result = mockMvc.perform(put(TESTED_ENDPOINT + "/" + entityFromResponse.getDeviceId()).with(registrationUser)
                .contentType(MediaType.APPLICATION_JSON)
                .content(TestUtils.mapObjectToJsonString(preparedDeviceBody)))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", notNullValue())).andReturn();

        log.info("After update response: {}", result.getResponse().getContentAsString());
        CreateDeviceRequestBody updatedEntityFromResponse = TestUtils.mapJsonToObject(result.getResponse().getContentAsString(), CreateDeviceRequestBody.class);
        assertEquals(preparedDeviceBody, updatedEntityFromResponse);
    }

    @Test
    void deleteEntitySuccess() throws Exception {
        var savedEntity = deviceRepository.saveAndFlush(ObjectMapper.mapToDevice(preparedDeviceBody));
        assertNotNull(savedEntity.getDeviceId());

        mockMvc.perform(delete(TESTED_ENDPOINT + "/" + savedEntity.getDeviceId()).with(registrationUser))
                .andExpect(status().isNoContent());
    }

    @AfterAll
    public static void removeTestData(@Autowired DeviceRepository deviceRepository) {
        deviceRepository.deleteInBatch(Arrays.stream(initDevices).collect(toList()));
    }
}


