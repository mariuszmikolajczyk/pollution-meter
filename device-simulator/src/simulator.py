import json
import random
from time import sleep

import pika
import requests
from requests.auth import HTTPBasicAuth

api_endpoint = "http://localhost:8090"
mq_host = 'localhost'
mq_port = 5672
mq_exchange_name = 'pollution-data'

mq_connection = pika.BlockingConnection(parameters=pika.ConnectionParameters(host=mq_host, port=mq_port,
                                                                             credentials=pika.credentials.PlainCredentials(
                                                                                 username='admin', password='admin')))
mq_channel = mq_connection.channel()


# mq_channel.queue_declare(queue=mq_queue_name)


def generate_mac():
    mac = [random.randint(0x00, 0x7f),
           random.randint(0x00, 0x7f),
           random.randint(0x00, 0x7f),
           random.randint(0x00, 0x7f),
           random.randint(0x00, 0xff),
           random.randint(0x00, 0xff)]

    return '-'.join(map(lambda x: "%02x" % x, mac))


class Location:
    def __init__(self, name, latitude, longitude):
        self.name = name
        self.longitude = longitude
        self.latitude = latitude


class Device:
    def __init__(self, mac_address, location):
        self.id = None
        self.mac_address = mac_address
        self.location = location

    def send_register_request(self):
        # longitude = random.randint(-18000, 18000) / 100
        # latitude = random.randint(-9000, 9000) / 100

        response = requests.post(api_endpoint + "/device", auth=HTTPBasicAuth('simulator', 'qwerty123'),
                                 json={
                                     "longitude": self.location.longitude,
                                     "latitude": self.location.latitude,
                                     "mac_address": self.mac_address,
                                     "location_name": self.location.name,
                                     "name": "simulator's device"
                                 })
        print(f"Create request's response: {response.json()}")
        if response.status_code == 409 and response.json()['details'] == 'Duplicated MAC address value':
            find_response = requests.get(api_endpoint + "/device/search?macAddress=" + self.mac_address,
                                         auth=HTTPBasicAuth('simulator', 'qwerty123'))
            print(f"Find response: {find_response.json()}")
            self.id = find_response.json()['device_id']
        else:
            self.id = response.json()['device_id']

    def send_measurement(self):
        pm25 = random.randint(1, 300)
        pm10 = random.randint(1, 500)

        measurement_json = {"device_id": '', "pm25": pm25, "pm10": pm10}
        mq_channel.basic_publish(exchange=mq_exchange_name, routing_key="",
                                 body=json.dumps(measurement_json))  # not tested
        print(f"Measurement send. Body: {measurement_json}")


#     main
devices = [
    Device("6d-1c-3c-56-5b-6e", Location('Miami', 25.761681, -80.191788)),
    Device("26-55-64-7f-3f-18", Location('New York City', 40.712776, -74.005974)),
    Device("3c-73-26-1f-a2-d9", Location('Washington DC', 38.907192, -77.036873)),
    Device("54-01-30-09-e5-0c", Location('Beijing', 39.904202, 116.407394)),
    Device("64-13-78-1a-e2-5f", Location('London', 51.507351, -0.127758))
]

# for d in devices:
#     d.send_register_request()
print("Devices registered\nStart sending data...")
# while True:
for d in devices:
    d.send_measurement()
sleep(10)
