package io.gitlab.mmikolajczyk.pollutionmeter.monitoringcomponent.rabbitmq

import com.fasterxml.jackson.databind.ObjectMapper
import io.gitlab.mmikolajczyk.pollutionmeter.monitoringcomponent.service.HealthStoreService
import org.slf4j.LoggerFactory
import org.springframework.amqp.rabbit.annotation.RabbitListener
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component
import java.util.concurrent.LinkedBlockingQueue

@Component
class PollutionDataListener(@Autowired private val objectMapper: ObjectMapper,
                            @Autowired private val healthStoreService: HealthStoreService) {
    private val dataCache = LinkedBlockingQueue<Long>()
    private var tempList = ArrayList<Long>(0)

    companion object {
        private val log = LoggerFactory.getLogger(PollutionDataListener::class.java)
    }

    @RabbitListener(queues = ["\${spring.rabbitmq.queue}"], returnExceptions = "true")
    fun onRabbitMessage(rawPayload: String) {
        val deviceId = objectMapper.readTree(rawPayload).get("device_id").asLong()
        log.debug("Device ID from message: {}", deviceId)

        dataCache.offer(deviceId)
    }

    @Scheduled(fixedDelayString = "\${batch-flushing-interval}")
    private fun flushDataInBatch() {
        dataCache.drainTo(tempList)
        for (deviceId in tempList)
            healthStoreService.putHeartbeat(deviceId)
        tempList.clear()
    }
}
