package io.gitlab.mmikolajczyk.pollutionmeter.monitoringcomponent.util

import com.hazelcast.partition.PartitionLostEvent
import org.slf4j.LoggerFactory

class PartitionLostListener : com.hazelcast.partition.PartitionLostListener {
    companion object {
        private val log = LoggerFactory.getLogger(PartitionLostListener::class.java)
    }

    override fun partitionLost(event: PartitionLostEvent?) {
        log.error("Lost partition detected! Details: {}", event)
    }
}
