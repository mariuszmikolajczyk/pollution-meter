package io.gitlab.mmikolajczyk.pollutionmeter.monitoringcomponent.web

import io.gitlab.mmikolajczyk.pollutionmeter.monitoringcomponent.service.HealthStoreService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/device/{deviceId}/health")
class DeviceHealthAPI(@Autowired private val healthStoreService: HealthStoreService) {

    @GetMapping
    fun checkHealth(@PathVariable("deviceId") deviceId: Long): HealthResponse {
        return HealthResponse(healthStoreService.getHealthForDevice(deviceId))
    }
}
