package io.gitlab.mmikolajczyk.pollutionmeter.monitoringcomponent.util

class HzConst {
    companion object {
        val YOUNG_DEVICE_MAP = "young-device"
        val OLD_DEVICE_MAP = "old-device"
    }
}
