package io.gitlab.mmikolajczyk.pollutionmeter.monitoringcomponent.service

import com.hazelcast.core.HazelcastInstance
import com.hazelcast.core.IMap
import io.gitlab.mmikolajczyk.pollutionmeter.monitoringcomponent.util.HzConst
import io.gitlab.mmikolajczyk.pollutionmeter.monitoringcomponent.web.HealthStatus
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service

@Service
class HealthStoreService(@Autowired private val hzInstance: HazelcastInstance,
                         @Value("\${store.young-device-adult-age}")
                         private val youngDeviceAdultAge: Int) {
    private val youngDeviceStore: IMap<Long, Int> = hzInstance.getMap<Long, Int>(HzConst.YOUNG_DEVICE_MAP)
    private val oldDeviceStore: IMap<Long, Boolean> = hzInstance.getMap<Long, Boolean>(HzConst.OLD_DEVICE_MAP)

    fun putHeartbeat(deviceId: Long) {
        if (oldDeviceStore.contains(deviceId)) {
            oldDeviceStore[deviceId] = true
            return
        }

        val youngAge = youngDeviceStore.getOrPut(deviceId) { 0 }
        if (youngAge >= youngDeviceAdultAge) {
            moveToOldStore(deviceId)
        } else {
            youngDeviceStore[deviceId] = youngAge + 1
        }
    }

    private fun moveToOldStore(deviceId: Long) {
        oldDeviceStore[deviceId] = true
        youngDeviceStore.remove(deviceId)
    }

    fun getHealthForDevice(deviceId: Long): HealthStatus {
        if (youngDeviceStore.containsKey(deviceId)) {
            return HealthStatus.ONLINE
        }
        if (oldDeviceStore.containsKey(deviceId)) {
            return HealthStatus.UP
        }
        return HealthStatus.DOWN
    }
}
