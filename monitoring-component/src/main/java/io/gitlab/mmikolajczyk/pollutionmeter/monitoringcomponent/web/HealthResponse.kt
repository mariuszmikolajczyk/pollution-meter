package io.gitlab.mmikolajczyk.pollutionmeter.monitoringcomponent.web

class HealthResponse(public val status: HealthStatus)

enum class HealthStatus {
    UP, DOWN, ONLINE
}
