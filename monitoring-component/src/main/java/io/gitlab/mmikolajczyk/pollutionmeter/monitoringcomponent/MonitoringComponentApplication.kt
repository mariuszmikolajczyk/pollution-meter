package io.gitlab.mmikolajczyk.pollutionmeter.monitoringcomponent

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.scheduling.annotation.EnableScheduling

@SpringBootApplication
@EnableScheduling
open class MonitoringComponentApplication() {
}

fun main(args: Array<String>) {
    runApplication<MonitoringComponentApplication>(*args)
}
