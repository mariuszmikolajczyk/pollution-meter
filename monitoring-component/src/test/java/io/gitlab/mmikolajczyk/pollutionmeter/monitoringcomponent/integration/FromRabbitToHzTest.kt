package io.gitlab.mmikolajczyk.pollutionmeter.monitoringcomponent.integration

import com.hazelcast.core.HazelcastInstance
import io.gitlab.mmikolajczyk.pollutionmeter.monitoringcomponent.util.HzConst
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory
import org.springframework.amqp.rabbit.core.RabbitTemplate
import org.springframework.amqp.rabbit.junit.BrokerRunningSupport
import org.springframework.amqp.rabbit.test.RabbitListenerTest
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.test.context.SpringBootTest


@SpringBootTest
@RabbitListenerTest
class FromRabbitToHzTest(brokerRunning: BrokerRunningSupport,
                         @Value("\${spring.rabbitmq.queue}") private val queueName: String,
                         @Autowired private val hzInstance: HazelcastInstance) {
    private val connectionFactory = CachingConnectionFactory(brokerRunning.connectionFactory)

    @Test
    internal fun putPollutionDataOnRabbitDeviceInYoungMap() {
        //given
        val rabbitTemplate = RabbitTemplate(connectionFactory)
        val msg = "{ \"device_id\":1}"

        //when
        rabbitTemplate.convertAndSend(queueName, msg)

        //then
        val youngMap = hzInstance.getMap<Long, Int>(HzConst.YOUNG_DEVICE_MAP)
        assertTrue(youngMap.containsKey(1))
    }
}
