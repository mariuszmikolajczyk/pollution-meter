package io.gitlab.mmikolajczyk.pollutionmeter.monitoringcomponent.service

import com.fasterxml.jackson.databind.ObjectMapper
import com.hazelcast.core.HazelcastInstance
import io.gitlab.mmikolajczyk.pollutionmeter.monitoringcomponent.util.HzConst
import io.gitlab.mmikolajczyk.pollutionmeter.monitoringcomponent.web.HealthStatus
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import org.springframework.boot.autoconfigure.amqp.RabbitAutoConfiguration
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import kotlin.test.assertEquals

//fixme persistence between test and hazelcast hardcoded port config (conflicts)
@SpringBootTest(properties = ["eureka.client.enabled=false"])
@EnableAutoConfiguration(exclude = [RabbitAutoConfiguration::class])
@AutoConfigureMockMvc
internal class HealthStoreServiceTest(@Autowired private val healthStoreService: HealthStoreService,
                                      @Autowired private val mockMvc: MockMvc,
                                      @Autowired private val objectMapper: ObjectMapper,
                                      @Value("\${store.young-device-adult-age}") private val youngDeviceAdultAge: Int,
                                      @Autowired private val hzInstance: HazelcastInstance) {

    @AfterEach
    fun clearHzMaps() {
        val youngMap = hzInstance.getMap<Long, Int>(HzConst.YOUNG_DEVICE_MAP)
        youngMap.clear()

        val oldMap = hzInstance.getMap<Long, Int>(HzConst.OLD_DEVICE_MAP)
        oldMap.clear()
    }

    @Test
    internal fun putSingleHeartbeatDeviceHasOnlineStatus() {
        //given
        val deviceId = 1L
        val status = getDeviceStatusFromREST(deviceId)
        assertEquals(HealthStatus.DOWN, status)

        //when
        healthStoreService.putHeartbeat(deviceId)

        //then
        val statusAfterHeartbeat = getDeviceStatusFromREST(deviceId)
        assertEquals(HealthStatus.ONLINE, statusAfterHeartbeat)
    }

    private fun getDeviceStatusFromREST(deviceId: Long): HealthStatus {
        val statusStr = objectMapper.readTree(mockMvc.perform(get("/device/${deviceId}/health"))
                .andExpect(status().isOk)
                .andReturn().response.contentAsString).get("status").asText()
        return HealthStatus.valueOf(statusStr)
    }

    @Test
    internal fun putMoreThanAdultThresholdHasUpStatus() {
        //given
        val deviceId = 2L
        val status = getDeviceStatusFromREST(deviceId)
        assertEquals(HealthStatus.DOWN, status)

        //when
        for (i in 0..youngDeviceAdultAge + 1) {
            healthStoreService.putHeartbeat(deviceId)
        }

        //then
        val statusAfterHeartbeat = getDeviceStatusFromREST(deviceId)
        assertEquals(HealthStatus.UP, statusAfterHeartbeat)
    }

    @Test
    internal fun notExistingDeviceIdHasStatusDown() {
        //given
        val deviceId = 15154545L

        //when
        val status = getDeviceStatusFromREST(deviceId)

        //then
        assertEquals(HealthStatus.DOWN, status)
    }
}
