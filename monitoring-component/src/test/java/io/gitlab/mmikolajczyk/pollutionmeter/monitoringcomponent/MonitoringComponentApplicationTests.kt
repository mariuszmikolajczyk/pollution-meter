package io.gitlab.mmikolajczyk.pollutionmeter.monitoringcomponent

import org.junit.jupiter.api.Test
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest
internal class MonitoringComponentApplicationTests {
    @Test
    fun contextLoads() {
    }
}
