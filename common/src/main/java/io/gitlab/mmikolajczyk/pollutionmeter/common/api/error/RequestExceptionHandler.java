package io.gitlab.mmikolajczyk.pollutionmeter.common.api.error;

import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.TypeMismatchException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.FieldError;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
@Slf4j
public class RequestExceptionHandler extends ResponseEntityExceptionHandler {

    /**
     * Methods handle errors from Bean Validation API
     */
    @Override
    public ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        FieldError fieldError = ex.getBindingResult().getFieldErrors().get(0);

        String details = "Field '" + fieldError.getField() + "' " + fieldError.getDefaultMessage();

        ErrorDetails errorDetails = ErrorDetailsFactory.buildErrorDetails(details);
        return new ResponseEntity<>(errorDetails, HttpStatus.BAD_REQUEST);
    }

    @Override
    public ResponseEntity<Object> handleExceptionInternal(Exception ex, Object body, HttpHeaders headers, HttpStatus status, WebRequest request) {
        log.error("Exception occured", ex);
        ErrorDetails errorDetails = ErrorDetailsFactory.buildErrorDetails("Internal error", null);
        return new ResponseEntity<>(errorDetails, status);
    }


    @ExceptionHandler({APIErrorException.class})
    public ResponseEntity handleCustomAPIError(APIErrorException exception) {
        return new ResponseEntity<>(ErrorDetailsFactory.buildErrorDetails(exception.getErrorMessage()), exception.getHttpStatus());
    }

    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        if (ex.getCause() instanceof InvalidFormatException) {
            InvalidFormatException invalidFormatException = (InvalidFormatException) ex.getCause();
            String fieldName = invalidFormatException.getPath().get(0).getFieldName();
            ErrorDetails errorDetails = ErrorDetailsFactory.buildErrorDetails("Field '" + fieldName + "' " + ErrorCodes.MessageSuffix.INCOMPATIBLE_VALUE);
            return new ResponseEntity<>(errorDetails, HttpStatus.UNPROCESSABLE_ENTITY);
        } else {
            return super.handleHttpMessageNotReadable(ex, headers, status, request);
        }
    }

    @Override
    protected ResponseEntity<Object> handleTypeMismatch(TypeMismatchException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        log.error("Type mismatch expection while parsing path param", ex);
        ErrorDetails errorDetails = ErrorDetailsFactory.buildErrorDetails(ErrorCodes.Message.UNABLE_TO_PARSE_PATH_PARAM.toString());
        return new ResponseEntity<>(errorDetails, HttpStatus.BAD_REQUEST);
    }

    @Override
    protected ResponseEntity<Object> handleHttpRequestMethodNotSupported(HttpRequestMethodNotSupportedException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        log.error("Method not supported exception", ex);
        ErrorDetails errorDetails = ErrorDetailsFactory.buildErrorDetails(String.format("Method '%s' with provided path param(s) and body is not supported", ex.getMethod()));
        return new ResponseEntity<>(errorDetails, HttpStatus.METHOD_NOT_ALLOWED);
    }
}
