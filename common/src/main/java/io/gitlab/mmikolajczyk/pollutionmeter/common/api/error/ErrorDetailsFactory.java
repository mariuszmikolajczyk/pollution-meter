package io.gitlab.mmikolajczyk.pollutionmeter.common.api.error;

import lombok.experimental.UtilityClass;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@UtilityClass
public class ErrorDetailsFactory {

    /**
     * Creates ErrorDetails instance which is used to clearly indicate error details in response to client
     * Example JSON response:
     * {
     * message: - briefly error message
     * details: - details of occured error (in other words - details whats was wrong)
     * timestamp: - readable date and time output of error occurrence
     * }
     */
    ErrorDetails buildErrorDetails(String message, String details) {
        return ErrorDetails.builder().message(message).details(details).timestamp(LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss.SSS"))).build();
    }

    /**
     * Creates ErrorDetails instance with default message='Invalid input!" for handling invalid value from client input
     */
    ErrorDetails buildErrorDetails(String details) {
        return buildErrorDetails("Invalid input!", details);
    }
}
