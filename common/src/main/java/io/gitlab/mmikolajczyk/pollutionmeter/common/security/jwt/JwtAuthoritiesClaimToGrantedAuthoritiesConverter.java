package io.gitlab.mmikolajczyk.pollutionmeter.common.security.jwt;

import org.springframework.core.convert.converter.Converter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.oauth2.server.resource.authentication.JwtGrantedAuthoritiesConverter;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Spring Security integration base only on SCOPE_ rules, not mapping authorities from JWT, which contains ROLE_ definitions
 */
public class JwtAuthoritiesClaimToGrantedAuthoritiesConverter implements Converter<Jwt, Collection<GrantedAuthority>> {
    private Converter<Jwt, Collection<GrantedAuthority>> jwtGrantedAuthoritiesConverter
            = new JwtGrantedAuthoritiesConverter();

    @Override
    public Collection<GrantedAuthority> convert(Jwt jwt) {
        Collection<GrantedAuthority> authorities = this.jwtGrantedAuthoritiesConverter.convert(jwt);
        List<String> customAuthorities = jwt.getClaim("authorities");

        authorities.addAll(customAuthorities.stream()
                .map(SimpleGrantedAuthority::new)
                .collect(Collectors.toSet()));

        return authorities;
    }
}
