package io.gitlab.mmikolajczyk.pollutionmeter.common.api.logger;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.util.ContentCachingRequestWrapper;
import org.springframework.web.util.ContentCachingResponseWrapper;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.stream.Collectors;

@Component
@Slf4j
public class RestLogger extends OncePerRequestFilter {

    private static ContentCachingResponseWrapper wrapResponse(HttpServletResponse response) {
        if (response instanceof ContentCachingResponseWrapper) {
            return (ContentCachingResponseWrapper) response;
        } else {
            return new ContentCachingResponseWrapper(response);
        }
    }

    //todo request is logged after its handling makes log appearance mismatch with real execution
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        if (isAsyncDispatch(request)) {
            filterChain.doFilter(request, response);
        } else {
            ContentCachingRequestWrapper wrappedRequest = new ContentCachingRequestWrapper(request);
            ContentCachingResponseWrapper wrappedResponse = wrapResponse(response);
            try {
                filterChain.doFilter(wrappedRequest, wrappedResponse);
            } finally {
                logRequest(wrappedRequest);
                logResponse(wrappedResponse, request);
            }
        }
    }

    private void logRequest(ContentCachingRequestWrapper request) {
        String body = getRequestBody(request);
        var parametersPart = request.getParameterMap().entrySet().stream().map(entry -> entry.getKey() + "=" + Arrays.toString(entry.getValue())).collect(Collectors.joining(","));
        log.info("Receive {} request to path \"{}\"", request.getMethod(), request.getRequestURI() + (!parametersPart.isEmpty() ? "?" + parametersPart : "") + (!body.isEmpty() ? " with body " + body : ""));
    }

    private void logResponse(ContentCachingResponseWrapper response, HttpServletRequest request) {
        log.info("Send response to {} method to path \"{}\" with status {} and body {}", request.getMethod(), request.getRequestURI(), response.getStatus() + " " + HttpStatus.valueOf(response.getStatus()).getReasonPhrase().toUpperCase(), getResponseBody(response));
        try {
            response.copyBodyToResponse();
        } catch (Exception e) {
            log.error("Error while copying response to cached response object", e);
        }
    }

    private String getRequestBody(ContentCachingRequestWrapper request) {
        try {
            return new String(request.getContentAsByteArray(), request.getCharacterEncoding());
        } catch (UnsupportedEncodingException e) {
            log.error("Error while reading request body", e);
        }
        return "";
    }

    private String getResponseBody(ContentCachingResponseWrapper response) {
        try {
            return new String(response.getContentAsByteArray(), response.getCharacterEncoding());
        } catch (UnsupportedEncodingException e) {
            log.error("Error while reading response body", e);
        }
        return "";
    }
}
