package io.gitlab.mmikolajczyk.pollutionmeter.common.api.error;

import lombok.Getter;
import lombok.experimental.UtilityClass;
import org.springframework.http.HttpStatus;

/**
 * Created for custom HTTP Status for different errors
 * <p>
 * Create message content and set proper HTTP status which should be returned to client in response of that error
 */
@Getter
public enum ErrorCodes {

    DEVICE_NOT_EXISTS(HttpStatus.NOT_FOUND, Message.DEVICE_NOT_EXISTS),
    UPDATE_DATA_ENDPOINT_NOT_ALLOWED(HttpStatus.FORBIDDEN, Message.UPDATE_DATA_ENDPOINT_NOT_ALLOWED),
    MAC_ADDRESS_DUPLICATE(HttpStatus.CONFLICT, Message.MAC_ADDRESS_DUPLICATE),
    DEVICE_MAC_ADDRESS_NOT_EXISTS(HttpStatus.NOT_FOUND, "Device with provided MAC address not found");

    private final HttpStatus httpStatus;
    private final String message;

    ErrorCodes(HttpStatus httpStatus, Message message) {
        this.httpStatus = httpStatus;
        this.message = message.value;
    }

    ErrorCodes(HttpStatus httpStatus, String message) {
        this.httpStatus = httpStatus;
        this.message = message;
    }

    @Override
    public String toString() {
        return message;
    }

    /**
     * Error message suffixes for generic error handling
     */
    @UtilityClass
    public class MessageSuffix {
        public final String INCOMPATIBLE_VALUE = "has incompatible value with expected value type";
        public final String NULL_VALUE = "cannot be null";
        public final String INCORRECT_VALUE = "has incorrect value";
    }

    /**
     * Error message reusable in tests
     */
    public enum Message {//todo to remove - below string can be accesses via ErrorCodes.getMessage() call
        DEVICE_NOT_EXISTS("Device with provided ID does not exists"),
        UNABLE_TO_PARSE_PATH_PARAM("Failed to parse path parameter's value(s)"),
        UPDATE_DATA_ENDPOINT_NOT_ALLOWED("Overwriting data_endpoint field not allowed"),
        MAC_ADDRESS_DUPLICATE("Duplicated MAC address value");

        private String value;

        Message(String value) {
            this.value = value;
        }

        @Override
        public String toString() {
            return value;
        }
    }
}
