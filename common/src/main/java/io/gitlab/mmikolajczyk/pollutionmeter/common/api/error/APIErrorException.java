package io.gitlab.mmikolajczyk.pollutionmeter.common.api.error;

import org.springframework.http.HttpStatus;

public class APIErrorException extends RuntimeException {
    private HttpStatus httpStatus;
    private String errorMessage;


    public APIErrorException(HttpStatus httpStatus, String errorMessage) {
        super(errorMessage);
        this.httpStatus = httpStatus;
        this.errorMessage = errorMessage;
    }

    public APIErrorException(ErrorCodes errorCodes) {
        super(errorCodes.getMessage());
        this.httpStatus = errorCodes.getHttpStatus();
        this.errorMessage = errorCodes.getMessage();
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    public String getErrorMessage() {
        return errorMessage;
    }
}
