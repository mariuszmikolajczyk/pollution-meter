package io.gitlab.mmikolajczyk.pollutionmeter.common.api.error;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class ErrorDetails {
    private String message;
    private String timestamp;
    private String details;
}
