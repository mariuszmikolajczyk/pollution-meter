package io.gitlab.mmikolajczyk.pollutionmeter.backend.avatar

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.core.io.UrlResource
import org.springframework.http.ResponseEntity
import org.springframework.http.codec.multipart.FilePart
import org.springframework.stereotype.Service
import reactor.core.publisher.Mono
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths

interface AvatarStorage {

    fun store(filename: String, avatar: Mono<FilePart>): Mono<Void>
    fun load(filename: String): Mono<ResponseEntity<UrlResource>>
}

@Service
class LocalDiskAvatarStorage(@Autowired storageProps: StorageProperties) : AvatarStorage {
    private val location: Path = Paths.get(storageProps.location)

    init {
        if (!Files.exists(location)) {
            Files.createDirectories(location)
        }
    }

    override fun store(filename: String, avatar: Mono<FilePart>): Mono<Void> {
        return avatar
                .flatMap { it.transferTo(location.resolve("$filename.jpg")) }
                .then(Mono.empty())
    }

    override fun load(filename: String): Mono<ResponseEntity<UrlResource>> {
        return Mono.just(ResponseEntity.ok(UrlResource(location.resolve(filename).toUri())))
                .onErrorReturn(ResponseEntity.notFound().build())
    }
}

@ConfigurationProperties(prefix = "avatar.storage")
class StorageProperties {
    lateinit var location: String
}
