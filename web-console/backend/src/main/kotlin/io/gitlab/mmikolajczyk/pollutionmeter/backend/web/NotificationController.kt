package io.gitlab.mmikolajczyk.pollutionmeter.backend.web

import io.gitlab.mmikolajczyk.pollutionmeter.backend.notification.NotificationService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController
import reactor.core.publisher.Flux
import java.time.Instant


@RestController
@CrossOrigin
class NotificationController {
    @Autowired
    private lateinit var notificationService: NotificationService

    @GetMapping("/API/notification", produces = [MediaType.TEXT_EVENT_STREAM_VALUE])
    fun streamNotification(): Flux<Notification> {
        return notificationService.streamNotifications()

//dev only
//        return Flux
//                .interval(Duration.ofSeconds(7))
//                .map { Notification("First notification", "If you see me it's mean notification stream work correctly :)") }
    }
}

data class Notification(val title: String, val content: String, val createdTime: Instant = Instant.now()) {

}
