package io.gitlab.mmikolajczyk.pollutionmeter.backend.notification

import com.fasterxml.jackson.databind.ObjectMapper
import io.gitlab.mmikolajczyk.pollutionmeter.backend.web.Notification
import org.slf4j.LoggerFactory
import org.springframework.amqp.rabbit.connection.ConnectionFactory
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import reactor.core.publisher.Flux
import reactor.core.publisher.FluxSink

@Service
class NotificationService {
    companion object {
        val log = LoggerFactory.getLogger(NotificationService.javaClass)
    }

    @Autowired
    private lateinit var connectionFactory: ConnectionFactory

    @Autowired
    private lateinit var objectMapper: ObjectMapper;

    @Value("\${rabbitmq.notification-queue-name}")
    private lateinit var queueName: String

    fun streamNotifications(): Flux<Notification> {
        val mlc = SimpleMessageListenerContainer(connectionFactory);
        mlc.addQueueNames(queueName)
        mlc.setDefaultRequeueRejected(false)

        return Flux.create { emitter: FluxSink<Notification> ->
            mlc.setupMessageListener { m ->
                val payload = String(m.body)
                log.trace("Got alert from queue = $payload")
                emitter.next(objectMapper.readValue(payload, Notification::class.java))
            }
            emitter.onRequest { mlc.start() }
            emitter.onDispose { mlc.stop() }
        }
    }
}
