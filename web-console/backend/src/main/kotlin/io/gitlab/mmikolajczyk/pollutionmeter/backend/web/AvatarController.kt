package io.gitlab.mmikolajczyk.pollutionmeter.backend.web

import io.gitlab.mmikolajczyk.pollutionmeter.backend.avatar.AvatarStorage
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.http.codec.multipart.FilePart
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.*
import reactor.core.publisher.Mono

@RestController
@RequestMapping("/API/avatar")
class AvatarController(@Autowired val storage: AvatarStorage) {

    @GetMapping("/{id}", produces = [MediaType.IMAGE_JPEG_VALUE])
    fun fetchAvatar(@PathVariable id: String) = storage.load("$id.jpg")


    @PutMapping(value = ["/{id}"], consumes = [MediaType.MULTIPART_FORM_DATA_VALUE])
    fun uploadAvatar(@PathVariable id: String, @RequestPart("file") @Validated file: Mono<FilePart>): Mono<Void> = storage.store(id, file)
}
