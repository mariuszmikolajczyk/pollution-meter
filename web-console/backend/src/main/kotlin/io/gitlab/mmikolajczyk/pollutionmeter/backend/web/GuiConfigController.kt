package io.gitlab.mmikolajczyk.pollutionmeter.backend.web

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/configuration")
class GuiConfigController {

    @Autowired
    private lateinit var guiConfiguration: GuiConfiguration

    @GetMapping
    fun getConfiguration(): GuiConfiguration {
        return guiConfiguration
    }
}

@Component
class GuiConfiguration(@Autowired val api_gateway: APIGatewayGuiProperties) {
}

@Component
class APIGatewayGuiProperties(@Autowired val credentials: APIGatewayCredentialsProperties, @Value("\${gui.config.api-gateway.endpoint}") val endpoint: String) {
}

@Component
class APIGatewayCredentialsProperties {
    @Value("\${gui.config.api-gateway.login}")
    lateinit var login: String
    @Value("\${gui.config.api-gateway.password}")
    lateinit var password: String
}
