import {Component, OnInit} from '@angular/core';
import {UserService} from '../service/user.service';
import {User} from '../model/user.model';
import {tap} from 'rxjs/operators';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  user: User;
  avatarError: boolean = false;
  avatarUrl;

  constructor(private userService: UserService) {
    this.user = userService.user();
    this.avatarUrl = '/API/avatar/' + this.user.name.toLowerCase();
    this.userService.reloadAvatarEmitter.subscribe(() => this.reloadAvatar());
  }

  ngOnInit() {
  }

  private reloadAvatar() {
    this.avatarUrl += '?updated=' + (new Date()).getTime();
  }

  uploadAvatar($event: Event) {
    // @ts-ignore
    this.userService.uploadAvatar($event.target.files[0])
      .pipe(
        tap(() => this.userService.reloadAvatarEmitter.emit())
      )
      .subscribe();
  }
}
