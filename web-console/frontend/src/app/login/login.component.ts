import {Component} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {environment} from '../../environments/environment';
import {UserService} from '../service/user.service';
import {HttpResponse} from '@angular/common/http';
import {tap} from 'rxjs/operators';
import {User} from '../model/user.model';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {
  form: FormGroup = new FormGroup({
    username: new FormControl(null, [Validators.required]),
    password: new FormControl(null, [Validators.required])
  });

  submitted = false;
  pending = false;
  inDemoSession = environment.demo;
  errorMsg: string = null;

  constructor(private router: Router, private loginService: UserService,
              private route: ActivatedRoute) {
  }

  onLogin() {
    this.submitted = true;
    if (this.form.valid === true) {
      const username = this.form.controls.username.value;
      const password = this.form.controls.password.value;

      const returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/home';

      if (this.inDemoSession) {
        if (!(username === 'demo' && password === 'demo')) {
          this.setErrorMsg('Invalid credentials!');
        }
        const demoUser = new User();
        demoUser.name = 'Demo User';
        demoUser.authorities = ['all'];
        localStorage.setItem(UserService.LS_USER_KEY, JSON.stringify(demoUser));

        this.router.navigate([returnUrl]);
        return;
      }
      this.pending = true;

      this.loginService.login(username, password)
        .pipe(tap(_ => this.pending = false))
        .subscribe(response => this.router.navigate([returnUrl]),
          (error: HttpResponse<any>) => {
            if (error.status === 401) {
              this.setErrorMsg('Invalid credentials!');
            }
            this.setErrorMsg('Unable to process login.');
            this.pending = false;
          });
    }
  }

  hasError(controlName: string) {
    if (!this.submitted) {
      return false;
    }
    return this.form.controls[controlName].errors !== null;
  }

  private setErrorMsg(msg: string) {
    this.errorMsg = msg;
  }
}
