import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {RouterModule} from '@angular/router';
import {HomeModule} from './home/home.module';
import {ResourcesModule} from './resources/resources.module';
import {ServiceModule} from './service/service.module';
import {ComponentsComponent} from './components/components.component';
import {BreadcrumbComponent} from './breadcrumb/breadcrumb.component';
import {MonitoringComponent} from './monitoring/monitoring.component';
import {AppRoutes} from './app-routing';
import {LoginModule} from './login/login.module';
import {NavbarModule} from './navbar/navbar.module';
import {AuthorizedGuard} from './service/authorized.guard';
import {ProfileComponent} from './profile/profile.component';
import {ToastsComponent} from './service/shared/toasts/toasts.component';
import {NgbToastModule} from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [
    AppComponent,
    ComponentsComponent,
    BreadcrumbComponent,
    MonitoringComponent,
    ProfileComponent,
    ToastsComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(AppRoutes.routes),
    HomeModule,
    ResourcesModule,
    ServiceModule,
    LoginModule,
    NavbarModule,
    NgbToastModule
  ],
  providers: [
    AuthorizedGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
