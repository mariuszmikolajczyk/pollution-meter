export class App {
  name: string;
  instanceCount: number;
  instances: Instance[];
}

export class Instance {
  homePageUrl: string;
  port: string;
  status: string;
  hostName: string;
  ipAddr: string;
  registered: Date;
}
