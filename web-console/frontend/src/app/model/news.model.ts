export class News {

  constructor(public title: string, public content: string, public author: string, public publishDate: string) {
  }
}
