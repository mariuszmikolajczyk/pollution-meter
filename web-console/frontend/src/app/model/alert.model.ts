export enum AlertLevel {
  INFO, DANGER, WARNING, SUCCESS
}

export class Alert {
  constructor(public content: string, public level: AlertLevel) {
  }
}
