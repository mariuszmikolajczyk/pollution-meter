export class Notification {


  constructor(public title: string, public content: string, public created_time: string, public unread: boolean = true) {
  }
}
