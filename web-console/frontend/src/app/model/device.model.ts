export enum DeviceStatus {
  UNKNOWN, UP, DOWN, ONLINE
}

export class Device {

  constructor(public device_id: number,
              public name: string,
              public created_date: string,
              public latitude: number,
              public longitude: number,
              public description: string,
              public location_name: string,
              public mac_address: string,
              public status: DeviceStatus = DeviceStatus.UNKNOWN) {
  }

}
