export class Authorization {
  constructor(public id: number, public macAddress: string, public validTo: string) {
  }
}
