//do not use slash (/) char at the end of path value
import {Routes} from '@angular/router';
import {LoginComponent} from './login/login.component';
import {HomeComponent} from './home/home.component';
import {DeviceComponent} from './resources/device/device.component';
import {DeviceInfoComponent} from './resources/device/device-info/device-info.component';
import {ComponentsComponent} from './components/components.component';
import {MonitoringComponent} from './monitoring/monitoring.component';
import {NavbarComponent} from './navbar/navbar.component';
import {AuthorizedGuard} from './service/authorized.guard';
import {ProfileComponent} from './profile/profile.component';

export class AppRoutes {
  public static readonly routes: Routes = [
    {path: 'login', component: LoginComponent},
    {
      path: '',
      component: NavbarComponent,
      canActivate: [AuthorizedGuard],
      children: [
        {path: 'home', component: HomeComponent},
        {path: 'device', component: DeviceComponent},
        {path: 'device/:id', component: DeviceInfoComponent},
        {path: 'components', component: ComponentsComponent},
        {path: 'monitoring', component: MonitoringComponent},
        {path: 'profile', component: ProfileComponent},
        {path: '**', redirectTo: 'home', pathMatch: 'full'}
      ]
    }
  ];
}
