import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {DeviceComponent} from './device/device.component';
import {DeviceListComponent} from './device/device-list/device-list.component';
import {DeviceAdminToolsComponent} from './device/device-admin-tools/device-admin-tools.component';
import {RouterModule} from '@angular/router';
import {ReactiveFormsModule} from '@angular/forms';
import {DeviceInfoComponent} from './device/device-info/device-info.component';


@NgModule({
  declarations: [DeviceComponent, DeviceListComponent, DeviceAdminToolsComponent, DeviceInfoComponent],
  imports: [
    CommonModule,
    RouterModule,
    ReactiveFormsModule
  ]
})
export class ResourcesModule { }
