import {Component, OnInit} from '@angular/core';
import {Device, DeviceStatus} from '../../../model/device.model';
import {DeviceService} from '../../../service/device.service';

@Component({
  selector: 'device-list',
  templateUrl: './device-list.component.html',
  styleUrls: ['./device-list.component.scss']
})
export class DeviceListComponent implements OnInit {
  devices: Device[] = [];
  loaded: boolean = false;
  apiError = false;

  constructor(private deviceService: DeviceService) {
  }

  ngOnInit() {
    this.deviceService.getAll().subscribe((value: Device[]) => {
        // value.map(it => Object.assign(new Device(), it)).forEach(it => this.devices.push(it));
        this.devices = value;
        this.loaded = true;
      },
      error => {
        this.loaded = true;
        this.apiError = true;
      });
  }

  getTableClass(status: DeviceStatus) {
    switch (status) {
      case DeviceStatus.UP:
        return 'table-success';
      case DeviceStatus.DOWN:
        return 'table-danger';
      case DeviceStatus.ONLINE:
        return 'table-warning';
      default:
        return '';
    }
  }

  getStatusBadge(status: DeviceStatus) {
    switch (status) {
      case DeviceStatus.UP:
        return 'badge-success';
      case DeviceStatus.DOWN:
        return 'badge-danger';
      case DeviceStatus.ONLINE:
        return 'badge-warning';
      default:
        return 'badge-secondary';
    }
  }

  getStatusName(status: DeviceStatus): string {
    if (status === undefined) {
      return DeviceStatus[DeviceStatus.UNKNOWN];
    }
    return DeviceStatus[status];
  }
}
