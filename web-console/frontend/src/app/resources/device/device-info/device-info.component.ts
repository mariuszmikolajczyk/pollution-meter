import {AfterViewInit, Component} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Device} from '../../../model/device.model';
import {DeviceService} from '../../../service/device.service';
import Map from 'ol/Map';
import View from 'ol/View';
import Feature from 'ol/Feature';
import OSM from 'ol/source/OSM';
import * as olProj from 'ol/proj';
import TileLayer from 'ol/layer/Tile';
import Point from 'ol/geom/Point';
import Style from 'ol/style/Style';
import Icon from 'ol/style/Icon';
import Vector from 'ol/source/Vector';
import VectorLayer from 'ol/layer/Vector';

@Component({
  selector: 'app-device-info',
  templateUrl: './device-info.component.html',
  styleUrls: ['./device-info.component.scss']
})
export class DeviceInfoComponent implements AfterViewInit {
  device: Device;
  apiError: boolean = false;
  loaded: boolean = false;
  private map: Map;

  constructor(private activatedRoute: ActivatedRoute, private deviceService: DeviceService) {
  }

  ngAfterViewInit(): void {
    this.deviceService.findById(Number(this.activatedRoute.snapshot.paramMap.get('id')))
      .subscribe((value: Device) => {
          this.device = value;
          this.loaded = true;
          this.initMap(value);
        },
        (error => {
          this.apiError = true;
          this.loaded = true;
        })
      );
  }

  private initMap(value: Device) {
    this.map = new Map({
      target: 'map',
      layers: [
        new TileLayer({
          source: new OSM()
        })
      ],
      view: new View({
        center: olProj.fromLonLat([value.longitude, value.latitude]),
        zoom: 15
      })
    });

    var iconFeature = new Feature({
      geometry: new Point(olProj.transform([value.longitude, value.latitude], 'EPSG:4326', 'EPSG:3857'))
    });

    var iconStyle = new Style({
      image: new Icon(({
        anchor: [0.5, 1],
        src: 'http://cdn.mapmarker.io/api/v1/pin?text=D&size=50&hoffset=1'
      }))
    });

    iconFeature.setStyle(iconStyle);


    var vectorSource = new Vector({
      features: [iconFeature]
    });

    var vectorLayer = new VectorLayer({
      source: vectorSource
    });
    this.map.addLayer(vectorLayer);

    setTimeout(() => this.map.updateSize(), 100);
  }

}
