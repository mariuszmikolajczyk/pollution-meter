import {NgModule} from '@angular/core';
import {DeviceService} from './device.service';
import {HTTP_INTERCEPTORS, HttpClient, HttpClientModule} from '@angular/common/http';
import {EurekaService} from './eureka.service';
import {environment} from '../../environments/environment';
import {StaticDeviceService} from './demo/static-device.service';
import {StaticEurekaService} from './demo/static-eureka.service';
import {UserService} from './user.service';
import {ElapsedPipe} from './shared/elapsed.pipe';
import {ToastService} from './toast.service';
import {APIOperationsInterceptor} from './api-operations.interceptor';


export function deviceServiceFactory(httpClient: HttpClient) {
  return environment.demo === true ? new StaticDeviceService() : new DeviceService(httpClient);
}

export function eurekaServiceFactory(httpClient: HttpClient) {
  return environment.demo === true ? new StaticEurekaService() : new EurekaService(httpClient);
}

@NgModule({
  imports: [HttpClientModule],
  providers: [
    {
      provide: DeviceService,
      useFactory: deviceServiceFactory,
      deps: [HttpClient]
    },
    {
      provide: EurekaService,
      useFactory: eurekaServiceFactory,
      deps: [HttpClient]
    },
    UserService,
    ToastService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: APIOperationsInterceptor,
      multi: true
    }],
  exports: [
    ElapsedPipe
  ],
  declarations: [ElapsedPipe]
})
export class ServiceModule {
}
