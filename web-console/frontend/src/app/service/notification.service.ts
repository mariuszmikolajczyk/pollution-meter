import {Injectable, NgZone} from '@angular/core';
import {Observable, of} from 'rxjs';
import {Notification} from '../model/notification.model';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {
  private static readonly STREAM_URL = 'http://localhost:8080/API/notification';

  constructor(private ngZone: NgZone) {
  }

  streamOfNotification(): Observable<Notification> {
    if (environment.demo) {
      return of(new Notification('Demo notification', 'This is notification content with details', new Date().toISOString()));
    }
    return new Observable(observer => {
      const eventSource = new EventSource(NotificationService.STREAM_URL);
      eventSource.onmessage = (event: any) => {
        this.ngZone.run(() => { //let UI be refreshed after push notification
          const notification = JSON.parse(event.data) as Notification;
          notification.unread = true;
          observer.next(notification);
        });
      };
      eventSource.onerror = error => {
          this.ngZone.run(() => {
            observer.error(error);
          });
        };
      }
    );
  }
}
