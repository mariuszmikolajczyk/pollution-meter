import {Observable} from 'rxjs';

export interface RESTfulService<T> {
  getAll(): Observable<T[]>;

  findById(id: number): Observable<T>;

  create(entity: T): Observable<T>;

  update(entity: T): Observable<T>;

  delete(entity: T);
}
