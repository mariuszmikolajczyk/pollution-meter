import {Injectable} from '@angular/core';


@Injectable()
export class ToastService {
  toasts: any[] = [];

  show(header: string, body: string, classname?: string) {
    this.toasts.push({header, body, classname});
  }

  remove(toast) {
    this.toasts = this.toasts.filter(t => t != toast);
  }

  showSuccess(header: string, body: string) {
    this.show(header, body, 'bg-success text-light');
  }

  showDanger(header: string, body: string) {
    this.show(header, body, 'bg-danger text-light');
  }
}
