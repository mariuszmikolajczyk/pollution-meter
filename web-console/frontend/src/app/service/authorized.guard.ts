import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree} from '@angular/router';
import {Observable} from 'rxjs';
import {Injectable} from '@angular/core';
import {UserService} from './user.service';

@Injectable()
export class AuthorizedGuard implements CanActivate {
  constructor(private router: Router) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    const isAuth = localStorage.getItem(UserService.LS_USER_KEY) !== null;
    if (!isAuth) {
      this.router.navigate(['/login'], {queryParams: {returnUrl: state.url}});
    }
    return isAuth;
  }
}
