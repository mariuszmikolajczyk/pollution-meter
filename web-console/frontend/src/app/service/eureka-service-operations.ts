import {App} from '../model/app.model';
import {Observable} from 'rxjs';

export interface EurekaServiceOperations {

  getAll(): Observable<App[]>;
}
