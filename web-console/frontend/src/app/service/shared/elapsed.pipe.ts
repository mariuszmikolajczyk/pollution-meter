import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'elapsed'
})
export class ElapsedPipe implements PipeTransform {
  private readonly divider: { value: number, repr: { single: string, plural: string } }[] = [
    {value: 1000, repr: {single: 'sec', plural: 'seconds'}},
    {value: 60, repr: {single: 'min', plural: 'mins'}},
    {value: 60, repr: {single: 'hr', plural: 'hrs'}},
    {value: 24, repr: {single: 'd', plural: 'days'}},
    {value: 30, repr: {single: 'month', plural: 'months'}},
    {value: 12, repr: {single: 'y', plural: 'yrs'}},
  ];

  transform(value: string, ...args: any[]): any {
    // @ts-ignore
    const elapsedMillis = new Date() - new Date(Date.parse(value));

    let dividerIndex = 0, lastPositiveValue = 1, divResult = elapsedMillis;
    do {
      divResult = Math.floor(divResult / this.divider[dividerIndex].value);
      if (divResult > 0) {
        lastPositiveValue = divResult;
      }
    } while (divResult > 0 && dividerIndex++ < this.divider.length);

    const lastDivider = this.divider[dividerIndex - 1].repr;
    return `${lastPositiveValue} ${(lastPositiveValue === 1 ? lastDivider.single : lastDivider.plural)} ago`;
  }
}
