import {Injectable} from '@angular/core';
import {HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {ToastService} from './toast.service';
import {catchError, tap} from 'rxjs/operators';


@Injectable()
export class APIOperationsInterceptor implements HttpInterceptor {

  constructor(private toastService: ToastService) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(req)
      .pipe(
        catchError((err: HttpErrorResponse) => {
          this.toastService.showDanger('Operation failed', err.message);
          return throwError(err);
        }),
        tap((res: HttpEvent<any>) => {
          if (res instanceof HttpResponse && req.method.toUpperCase() !== 'GET') {
            this.toastService.showSuccess('Operation successed', 'Operation completed successfully');
          }
        })
      );
  }

}
