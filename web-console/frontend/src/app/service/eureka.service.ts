import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {App, Instance} from '../model/app.model';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {GatewayBaseRestService} from './gateway-base-rest.service';
import {EurekaServiceOperations} from './eureka-service-operations';

@Injectable()
export class EurekaService extends GatewayBaseRestService implements EurekaServiceOperations {

  constructor(httpClient: HttpClient) {
    super(httpClient);
  }

  getAll(): Observable<App[]> {
    const result: App[] = [];
    return this.performGet<App[]>('/backend/eureka/apps').pipe(
      map((res: any) => {
        const appsJson = (res.applications.application as []);
        appsJson.forEach((arrElement: any) => {
          const app = new App();
          app.name = arrElement.name;
          app.instanceCount = arrElement.instance.length;

          const instances: Instance[] = [];
          arrElement.instance.forEach(inst => {
            const instanceModel = new Instance();
            instanceModel.homePageUrl = inst.homePageUrl;
            instanceModel.hostName = inst.hostName;
            instanceModel.ipAddr = inst.ipAddr;
            instanceModel.port = inst.port.$;
            instanceModel.registered = new Date(inst.leaseInfo.registrationTimestamp);
            instanceModel.status = inst.status;
            instances.push(instanceModel);
          });
          app.instances = instances;
          result.push(app);
        });
        return result;
      }));
  }
}
