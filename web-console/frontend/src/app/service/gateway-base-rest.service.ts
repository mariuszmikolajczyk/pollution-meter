import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';


export abstract class GatewayBaseRestService {
  private defaultOptions = {withCredentials: true};

  constructor(protected httpClient: HttpClient) {

  }

  performGet<T>(uri: string, options: {} = this.defaultOptions): Observable<T> {
    return this.httpClient.get<T>(uri, options);
  }

  performPost<T>(uri: string, body: {}, options: {} = this.defaultOptions): Observable<T> {
    return this.httpClient.post<T>(uri, body, options);
  }

}
