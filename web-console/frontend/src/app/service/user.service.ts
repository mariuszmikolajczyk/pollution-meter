import {EventEmitter, Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {GatewayBaseRestService} from './gateway-base-rest.service';
import {User} from '../model/user.model';
import {flatMap, tap} from 'rxjs/operators';


@Injectable()
export class UserService extends GatewayBaseRestService {
  static readonly LS_USER_KEY: string = 'authorized';
  reloadAvatarEmitter: EventEmitter<void> = new EventEmitter<void>();

  constructor(httpClient: HttpClient) {
    super(httpClient);
  }

  login(username: string, password: string): Observable<User> {
    return this.httpClient.post<HttpResponse<any>>('/backend/login', 'username=' + username + '&password=' + password, {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      withCredentials: true
    })
      .pipe(
        flatMap(noMatterIfIsOk => this.getAndStoreAuthorization())
      );
  }

  user(): User {
    return JSON.parse(localStorage.getItem(UserService.LS_USER_KEY));
  }

  logout(): Observable<HttpResponse<any>> {
    localStorage.removeItem(UserService.LS_USER_KEY);
    return this.performPost('/backend/logout', {});
  }

  private getAndStoreAuthorization() {
    return this.performGet<User>('/backend/user')
      .pipe(
        tap((loggedUser: User) => localStorage.setItem(UserService.LS_USER_KEY, JSON.stringify(loggedUser)))
      );
  }

  uploadAvatar(avatar: File) {
    const formData = new FormData();
    formData.append('file', avatar);
    return this.httpClient.put(`/API/avatar/${this.user().name.toLowerCase()}`, formData);
  }

}
