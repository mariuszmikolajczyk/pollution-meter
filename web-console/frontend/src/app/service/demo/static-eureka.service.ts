import {EurekaServiceOperations} from '../eureka-service-operations';
import {Observable, of} from 'rxjs';
import {App, Instance} from '../../model/app.model';

export class StaticEurekaService implements EurekaServiceOperations {
  private apps: App[];

  constructor() {
    const instance = new Instance();
    instance.status = 'UP';
    instance.homePageUrl = 'https://registration.pollution-meter.gitlab.io';
    instance.ipAddr = '10.334.120.2';
    instance.port = '66790';
    instance.registered = new Date();

    const app = new App();
    app.name = 'REGISTER-COMPONENT';
    app.instanceCount = 1;
    app.instances = [instance];

    this.apps = [app];
  }

  getAll(): Observable<App[]> {
    return of(this.apps);
  }
}
