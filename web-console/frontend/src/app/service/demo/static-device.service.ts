import {RESTfulService} from '../RESTful.service';
import {Device, DeviceStatus} from '../../model/device.model';
import {Observable, of} from 'rxjs';

export class StaticDeviceService implements RESTfulService<Device> {
  private devices = [new Device(1, 'Device #1', '2019-09-22 14:45:11', 40.712776, -74.005974, '', 'NYC', '00-ff-03-ac-90-e4', DeviceStatus.UP)];

  create(entity: Device): Observable<Device> {
    return undefined;
  }

  delete(entity: Device) {
  }

  findById(id: number): Observable<Device> {
    return of(this.devices.filter(d => d.device_id === id)[0]);
  }

  getAll(): Observable<Device[]> {
    return of(this.devices);
  }

  update(entity: Device): Observable<Device> {
    return undefined;
  }
}
