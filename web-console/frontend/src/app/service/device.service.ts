import {HttpClient} from '@angular/common/http';
import {RESTfulService} from './RESTful.service';
import {Device} from '../model/device.model';
import {Observable} from 'rxjs';
import {Injectable} from '@angular/core';
import {GatewayBaseRestService} from './gateway-base-rest.service';

@Injectable()
export class DeviceService extends GatewayBaseRestService implements RESTfulService<Device> {
  private endpoint = '/backend/register-component/device';

  constructor(httpClient: HttpClient) {
    super(httpClient);
  }

  create(entity: Device): Observable<Device> {
    return this.httpClient.post<Device>(this.endpoint, entity);
  }

  delete(entity: Device) {
    return this.httpClient.delete(this.endpoint + '/' + entity.device_id);
  }

  findById(id: number): Observable<Device> {
    return this.performGet<Device>(this.endpoint + '/' + id);
  }

  getAll(): Observable<Device[]> {
    return this.performGet<Device[]>(this.endpoint);
  }

  update(entity: Device): Observable<Device> {
    return this.httpClient.put<Device>(this.endpoint, entity);
  }
}
