import {Component, OnInit} from '@angular/core';
import {News} from '../../model/news.model';

@Component({
  selector: 'news-feed',
  templateUrl: './news-feed.component.html',
  styleUrls: ['./news-feed.component.scss']
})
export class NewsFeedComponent implements OnInit {
  news: News[] = [
    new News('Welcome!', 'Welcome to web application to manage Pollution meter\' application', 'Main Administrator', 'Today')
  ];

  constructor() {
  }

  ngOnInit() {
  }

}
