import {Component, OnInit} from '@angular/core';
import {Alert, AlertLevel} from '../model/alert.model';
import {QuickStatsModel} from '../model/quick-stats.model';
import {DeviceService} from '../service/device.service';
import {Device} from '../model/device.model';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  alerts: Alert[] = [];
  quickStats: QuickStatsModel = new QuickStatsModel();

  constructor(private deviceService: DeviceService) {
  }

  ngOnInit() {
    this.deviceService.getAll().subscribe((value: Device[]) => {
      this.quickStats.allDeviceCount = value.length;
    });
  }

}
