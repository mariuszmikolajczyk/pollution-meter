import {Component, OnInit} from '@angular/core';
import {NotificationService} from '../../service/notification.service';
import {Notification} from '../../model/notification.model';
import {tap} from 'rxjs/operators';
import {ToastService} from '../../service/toast.service';

@Component({
  selector: 'app-notification-container',
  templateUrl: './notification-container.component.html',
  styleUrls: ['./notification-container.component.scss']
})
export class NotificationContainerComponent implements OnInit {
  notification: Notification[] = [];
  unreadCount: number = 0;

  constructor(private notificationService: NotificationService, private toastService: ToastService) {
  }

  ngOnInit() {
    this.notificationService.streamOfNotification()
      .pipe(
        tap(notification => {
          this.notification.unshift(notification);
          this.unreadCount++;
          this.toastService.show(notification.title, notification.content);
        })
      )
      .subscribe();
  }

  readAll() {
    this.notification.forEach(n => n.unread = false);
    this.unreadCount = 0;
  }

  markAsReaded(n: Notification) {
    n.unread = false;
    this.unreadCount--;
  }
}
