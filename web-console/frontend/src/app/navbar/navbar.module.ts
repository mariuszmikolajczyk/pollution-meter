import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {NavbarComponent} from './navbar.component';
import {RouterModule} from '@angular/router';
import {NotificationContainerComponent} from './notification-container/notification-container.component';
import {ServiceModule} from '../service/service.module';


@NgModule({
  declarations: [NavbarComponent, NotificationContainerComponent],
  imports: [
    CommonModule,
    RouterModule,
    ServiceModule,
    // RouterModule.forChild(NavRouting.routes)
  ]
})
export class NavbarModule {
}
