import {Component, OnInit} from '@angular/core';
import {environment} from '../../environments/environment';
import {UserService} from '../service/user.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  inDemoSession = environment.demo;
  username: string = this.inDemoSession ? 'Demo User' : '';
  avatarError: boolean = false;
  avatarUrl;

  constructor(private userService: UserService, private router: Router) {
    this.avatarUrl = '/API/avatar/' + this.userService.user().name.toLowerCase();
    this.userService.reloadAvatarEmitter.subscribe(() => this.avatarUrl += '?updated=' + (new Date()).getTime());
  }

  ngOnInit() {
    if (!this.inDemoSession) {
      this.username = this.userService.user().name;
    }
  }

  logout() {
    this.userService.logout().subscribe(() => this.router.navigateByUrl('/login?logout'));
  }

}
