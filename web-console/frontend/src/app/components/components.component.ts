import {Component, OnInit} from '@angular/core';
import {EurekaService} from '../service/eureka.service';
import {App} from '../model/app.model';

@Component({
  selector: 'app-components',
  templateUrl: './components.component.html',
  styleUrls: ['./components.component.scss']
})
export class ComponentsComponent implements OnInit {
   apps: App[] = [];
  private apiError: boolean = false;

  constructor(private eurekaService: EurekaService) {
  }

  ngOnInit() {
    this.eurekaService.getAll().subscribe(value => {
        this.apps = value;
      },
      error => {
        this.apiError = true;
        console.log(error);
      }
    );
  }

}
