import {Component, OnInit} from '@angular/core';
import {DeviceService} from '../service/device.service';

@Component({
  selector: 'app-monitoring',
  templateUrl: './monitoring.component.html',
  styleUrls: ['./monitoring.component.scss']
})
export class MonitoringComponent implements OnInit {
  deviceCount: string | number = 'N/A';
  authorizationCount: string | number = 'N/A';

  constructor(private deviceService: DeviceService) {
  }

  ngOnInit() {
    this.deviceService.getAll().subscribe(value => this.deviceCount = value.length);
  }

}
