const PROXY_CONFIG = [
  {
    context: [
      "/backend/**",
      "/logout/**",
      "/API/**"
    ],
    target: "http://localhost:8090",
    secure: false,
    log: "debug",
    pathRewrite: {
      "^/backend/": ""
    }
  }
]

module.exports = PROXY_CONFIG;
