package io.gitlab.mmikolajczyk.pollutionmeter.measurementcomponent.controller

import io.gitlab.mmikolajczyk.pollutionmeter.measurementcomponent.entity.Measurement
import io.gitlab.mmikolajczyk.pollutionmeter.measurementcomponent.repository.MeasurementRepository
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/measurement")
class MeasurementAPI(private val repository: MeasurementRepository) {

    @GetMapping
    fun getMeasurement(): List<Measurement> {
        return repository.findAll()
    }
}
