package io.gitlab.mmikolajczyk.pollutionmeter.measurementcomponent.util

import com.fasterxml.jackson.databind.ObjectMapper
import io.gitlab.mmikolajczyk.pollutionmeter.measurementcomponent.entity.Measurement
import org.springframework.stereotype.Component

@Component
class MeasurementFactory(private val objectMapper: ObjectMapper) {

    fun fromJsonValue(rawMeasurement: String): Measurement = objectMapper.readValue(rawMeasurement, Measurement::class.java)
}
