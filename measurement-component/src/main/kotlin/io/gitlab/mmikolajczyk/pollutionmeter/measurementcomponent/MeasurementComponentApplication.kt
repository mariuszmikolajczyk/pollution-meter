package io.gitlab.mmikolajczyk.pollutionmeter.measurementcomponent

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class MeasurementComponentApplication

fun main(args: Array<String>) {
    runApplication<MeasurementComponentApplication>(*args)
}
