package io.gitlab.mmikolajczyk.pollutionmeter.measurementcomponent.queue

import io.gitlab.mmikolajczyk.pollutionmeter.measurementcomponent.repository.MeasurementRepository
import io.gitlab.mmikolajczyk.pollutionmeter.measurementcomponent.util.MeasurementFactory
import org.slf4j.LoggerFactory
import org.springframework.amqp.rabbit.annotation.RabbitListener
import org.springframework.stereotype.Service

@Service
class QueueListener(private val repository: MeasurementRepository, private val measurementFactory: MeasurementFactory) {

    companion object {
        val log = LoggerFactory.getLogger(QueueListener::class.java)
    }

    @RabbitListener(queues = ["\${rabbitmq.queue-name}"], returnExceptions = "true")
    fun consumeQueue(message: String) {
        log.trace("Readed from queue ${message}")
        repository.save(measurementFactory.fromJsonValue(message))
    }
}

