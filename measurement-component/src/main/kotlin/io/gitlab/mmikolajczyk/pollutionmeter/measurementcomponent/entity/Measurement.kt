package io.gitlab.mmikolajczyk.pollutionmeter.measurementcomponent.entity

import com.fasterxml.jackson.annotation.JsonAnyGetter
import com.fasterxml.jackson.annotation.JsonAnySetter
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document
import java.util.*

@Document
class Measurement {
    @Id
    var id: String = UUID.randomUUID().toString()
    var data = HashMap<String, Any>()
        @JsonAnyGetter get

    @JsonAnySetter
    fun addData(key: String, value: Any) {
        data.put(key, value)
    }

}
