package io.gitlab.mmikolajczyk.pollutionmeter.measurementcomponent.repository

import io.gitlab.mmikolajczyk.pollutionmeter.measurementcomponent.entity.Measurement
import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.stereotype.Repository

@Repository
interface MeasurementRepository : MongoRepository<Measurement, Int> {

}
