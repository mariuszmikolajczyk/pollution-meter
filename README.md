![](doc/logo/pollution-meter-logo-ext-sm.png "project's logo")

Pollution meter is a conceptional project for gathering pollution data from many IoT-like devices with pollution sensors from any location.

## How it works ?
In below picture there is shortly presented how pollution data from devices are handled in this system:
![alt text](doc/img/device-handling-step-by-step.png?raw=true "device-how-to-step-by-step")

## Technology stack
* Java
* Spring (Boot, MVC, Data, Security, AMQP, Cloud)
* Angular
* PostgreSQL database + Liquibase 
* MongoDB
* RabbitMQ Message Broker
* Junit 5 (for integration tests)
* Docker (with docker-compose tool)
* Maven

## Architecture
Project components' granulation were performed using a microservices' architecture. To orchestrate such services there were used Netflix OSS tools with Spring Cloud support for discovering and load balancing between their available instances.
By using that software architecture, system's infrastructure can handle huge network traffic with a lot of requests when number of devices will be increasing.

## Components
#### Register component
Component is responsible for registration new devices in the system.

#### Measurement component
Component is responsible for consume data from message broker's queue and store measurement data into the database.

#### Web console
Web application allows to preview information about system i.a. existing devices and measurements. Website allows also to manage system resources and monitor running components.

**For extended documentation of all components and project, please visit [project's Wiki](https://gitlab.com/mariuszmikolajczyk/pollution-meter/wikis/About-project)**

## Live demo of Web console GUI
**Visit: https://mariuszmikolajczyk.gitlab.io/pollution-meter**

## Plans to extend project features
* process stored data for statistics and prediction of pollution in specified localization (Apache Spark, AI/ML algorithms)
