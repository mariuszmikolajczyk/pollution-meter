package io.gitlab.mmikolajczyk.pollutionmeter.apigateway.web;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

import static java.util.stream.Collectors.toSet;

@RestController
@RequestMapping("/user")
public class UserEndpoint {

    @GetMapping
    public Map<String, Object> userInfo(@AuthenticationPrincipal OAuth2AuthenticationToken authenticated) {
        return Map.of("name", authenticated.getName(), "authorities", authenticated.getAuthorities().stream().map(GrantedAuthority::getAuthority).collect(toSet()));
    }
}
