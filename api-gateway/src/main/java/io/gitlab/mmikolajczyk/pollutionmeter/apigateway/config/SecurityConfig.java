package io.gitlab.mmikolajczyk.pollutionmeter.apigateway.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.web.server.SecurityWebFiltersOrder;
import org.springframework.security.config.web.server.ServerHttpSecurity;
import org.springframework.security.web.server.SecurityWebFilterChain;
import org.springframework.security.web.server.authentication.logout.DelegatingServerLogoutHandler;
import org.springframework.security.web.server.authentication.logout.HeaderWriterServerLogoutHandler;
import org.springframework.security.web.server.authentication.logout.RedirectServerLogoutSuccessHandler;
import org.springframework.security.web.server.authentication.logout.SecurityContextServerLogoutHandler;
import org.springframework.security.web.server.context.WebSessionServerSecurityContextRepository;
import org.springframework.security.web.server.header.StaticServerHttpHeadersWriter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.reactive.CorsConfigurationSource;
import org.springframework.web.cors.reactive.UrlBasedCorsConfigurationSource;

import java.net.URI;
import java.util.List;

import static org.springframework.security.config.Customizer.withDefaults;

@Configuration
@Slf4j
public class SecurityConfig {

    private final WebSessionServerSecurityContextRepository securityContextRepository;
    private final ForcedOAuth2PasswordFlowFilter passwordFilter;

    public SecurityConfig(WebSessionServerSecurityContextRepository securityContextRepository, ForcedOAuth2PasswordFlowFilter passwordFilter) {
        this.securityContextRepository = securityContextRepository;
        this.passwordFilter = passwordFilter;
    }

    @Bean
    public SecurityWebFilterChain securityWebFilterChain(ServerHttpSecurity http) {
        final var LOGOUT_URL = "/logout/ok";
        RedirectServerLogoutSuccessHandler redirectHandler = new RedirectServerLogoutSuccessHandler();
        redirectHandler.setLogoutSuccessUrl(URI.create(LOGOUT_URL));

        return http
                .securityContextRepository(securityContextRepository)
                .addFilterAt(passwordFilter, SecurityWebFiltersOrder.AUTHORIZATION)
                .authorizeExchange(exchanges -> exchanges
                        .pathMatchers("/login", LOGOUT_URL).permitAll()
                        .anyExchange().authenticated()
                )
                .logout(logout -> {
                    logout.logoutSuccessHandler(redirectHandler)
                            .logoutHandler(new DelegatingServerLogoutHandler(new SecurityContextServerLogoutHandler(),
                                    new HeaderWriterServerLogoutHandler(StaticServerHttpHeadersWriter.builder().header("Clear-Site-Data", "\"cookies\"").build()))
                            );
                })

                .csrf(ServerHttpSecurity.CsrfSpec::disable)
                .cors(withDefaults())
                .build();
    }

    @Bean
    public CorsConfigurationSource corsConfigurationSource(@Value("${cors.allowed-origins}") List<String> allowedOrigins) {
        final CorsConfiguration config = new CorsConfiguration();
        config.setAllowCredentials(true);
        config.setAllowedOrigins(allowedOrigins);
        config.addAllowedHeader("*");
        config.setAllowedMethods(List.of("OPTIONS", "GET", "POST", "PUT", "DELETE"));

        final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", config);
        return source;
    }
}

