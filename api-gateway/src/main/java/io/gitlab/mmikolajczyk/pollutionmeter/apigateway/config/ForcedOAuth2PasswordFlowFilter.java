package io.gitlab.mmikolajczyk.pollutionmeter.apigateway.config;

import com.fasterxml.jackson.databind.node.ObjectNode;
import io.gitlab.mmikolajczyk.pollutionmeter.common.security.jwt.JwtAuthoritiesClaimToGrantedAuthoritiesConverter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.ReactiveSecurityContextHolder;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextImpl;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClient;
import org.springframework.security.oauth2.client.ReactiveOAuth2AuthorizedClientService;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.security.oauth2.client.registration.ReactiveClientRegistrationRepository;
import org.springframework.security.oauth2.core.OAuth2AccessToken;
import org.springframework.security.oauth2.core.OAuth2RefreshToken;
import org.springframework.security.oauth2.core.user.DefaultOAuth2User;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.oauth2.jwt.JwtDecoder;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationConverter;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;
import org.springframework.security.web.server.context.ServerSecurityContextRepository;
import org.springframework.stereotype.Component;
import org.springframework.util.Base64Utils;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.server.ServerWebExchange;
import org.springframework.web.server.WebFilter;
import org.springframework.web.server.WebFilterChain;
import reactor.core.publisher.Mono;

import java.time.Instant;
import java.util.stream.Collectors;

@Component
@Slf4j
//todo refactor using own ReactiveAuthenticationManager implementation
class ForcedOAuth2PasswordFlowFilter implements WebFilter {
    private final ReactiveOAuth2AuthorizedClientService clientService;
    private final ReactiveClientRegistrationRepository clientRegistrationRepository;
    private final ServerSecurityContextRepository securityContextRepository;
    private final JwtDecoder jwtDecoder;
    private final JwtAuthenticationConverter converter = new JwtAuthenticationConverter();
    private final JwtAuthoritiesClaimToGrantedAuthoritiesConverter authoritiesConverter = new JwtAuthoritiesClaimToGrantedAuthoritiesConverter();

    @Autowired
    public ForcedOAuth2PasswordFlowFilter(ReactiveOAuth2AuthorizedClientService clientService, ReactiveClientRegistrationRepository clientRegistrationRepository,
                                          ServerSecurityContextRepository securityContextRepository, JwtDecoder jwtDecoder) {
        this.clientService = clientService;
        this.clientRegistrationRepository = clientRegistrationRepository;
        this.securityContextRepository = securityContextRepository;
        this.jwtDecoder = jwtDecoder;
    }

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, WebFilterChain chain) {
//todo rewrite with proper RxJava transform methods...
        return ReactiveSecurityContextHolder.getContext()
                .switchIfEmpty(Mono.defer(() -> {
                    if (exchange.getRequest().getMethod().equals(HttpMethod.POST) && exchange.getRequest().getURI().getPath().equals("/login")) {
                        log.debug("Starting...");
                        SecurityContext securityContext = new SecurityContextImpl();
                        clientRegistrationRepository.findByRegistrationId("gateway").doOnNext(client -> {
                            log.debug("Trying to authorize user...");
                            exchange.getFormData().doOnNext(parms -> {
                                WebClient.create()
                                        .post()
                                        .uri(client.getProviderDetails().getTokenUri())
                                        .header("Authorization", "Basic " + Base64Utils.encodeToString((client.getClientId() + ":" + client.getClientSecret()).getBytes()))
                                        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                                        .bodyValue("username=" + parms.get("username").get(0) + "&password=" + parms.get("password").get(0) + "&grant_type=password&scope=read write")
                                        .retrieve()
                                        .bodyToMono(ObjectNode.class).doOnNext(res -> {
                                    log.debug("Retrieve token, parsing...");
                                    final Jwt jwt = jwtDecoder.decode(res.get("access_token").asText());
                                    converter.setJwtGrantedAuthoritiesConverter(authoritiesConverter);
                                    JwtAuthenticationToken authentication = (JwtAuthenticationToken) converter.convert(jwt);
                                    OAuth2AuthenticationToken oAuth2Authentication = new OAuth2AuthenticationToken(new DefaultOAuth2User(authentication.getAuthorities(), authentication.getTokenAttributes(), "sub"),
                                            authentication.getAuthorities(), client.getRegistrationId());
                                    securityContext.setAuthentication(oAuth2Authentication);

                                    clientService.saveAuthorizedClient(
                                            new OAuth2AuthorizedClient(client, authentication.getName(),
                                                    new OAuth2AccessToken(OAuth2AccessToken.TokenType.BEARER,
                                                            jwt.getTokenValue(),
                                                            jwt.getIssuedAt(),
                                                            jwt.getExpiresAt(),
                                                            authentication.getAuthorities().stream().map(GrantedAuthority::getAuthority).collect(Collectors.toSet())),
                                                    new OAuth2RefreshToken(res.get("refresh_token").asText(), Instant.now())), authentication)
                                            .subscribe();

                                    securityContextRepository.save(exchange, securityContext).subscribe();
                                    log.debug("Authorized successfully");
                                }).doOnError(throwable -> {
                                    log.error("Exception during authorization request", throwable);
                                    exchange.getResponse().setStatusCode(HttpStatus.BAD_REQUEST);
                                })
                                        .subscribe();
                            }).subscribe();
                        }).subscribe();

                        try {
                            Thread.sleep(750);//fixme !!!!! causing 500 internal errors !!!!!!!!!!!!!
                        } catch (InterruptedException e) {
                            log.error("Sleep interrupted", e);
                        }

                        return chain.filter(exchange)
                                .subscriberContext(ReactiveSecurityContextHolder.withSecurityContext(Mono.just(securityContext)))
                                .then(Mono.empty());
                    } else {
                        log.debug("Skipped");
                        return chain.filter(exchange)
                                .then(Mono.empty());
                    }
                })).flatMap(securityContext -> chain.filter(exchange));
    }
}
