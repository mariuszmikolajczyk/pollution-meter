package io.gitlab.mmikolajczyk.pollutionmeter.apigateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.HttpEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.Map;

@SpringBootApplication
@RestController
public class ApiGatewayApplication {

    public static void main(String[] args) {
        SpringApplication.run(ApiGatewayApplication.class, args);
    }

    @PostMapping("login")
    public void dummyEndpointForFilter() {
    }

    @GetMapping("/logout/ok")
    public HttpEntity ok() {
        return new HttpEntity(new LinkedMultiValueMap<>(
                Map.of("Set-Cookie", Collections.singletonList("SESSION=; path=/; expires=Thu, 01 Jan 1970 00:00:00 GMT"))));//invalidate SESSION cookie
    }
}
