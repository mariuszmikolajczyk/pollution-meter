package io.gitlab.mmikolajczyk.pollutionmeter.queuecomponent.model.mapper;

import io.gitlab.mmikolajczyk.pollutionmeter.queuecomponent.controller.body.MeasurementBody;
import io.gitlab.mmikolajczyk.pollutionmeter.queuecomponent.model.MeasurementDTO;

public class MeasurementMapper {

    public static MeasurementDTO mapFromRequestBody(MeasurementBody body) {
        MeasurementDTO dtoObject = new MeasurementDTO();
        dtoObject.setId(body.getId());
        dtoObject.setPm25(body.getPm25());
        dtoObject.setPm10(body.getPm10());
        return dtoObject;
    }
}
