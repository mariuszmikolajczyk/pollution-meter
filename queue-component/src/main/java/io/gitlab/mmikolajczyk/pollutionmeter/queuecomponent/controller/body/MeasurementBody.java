package io.gitlab.mmikolajczyk.pollutionmeter.queuecomponent.controller.body;

import lombok.Getter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

@Getter
public class MeasurementBody {

    @NotNull
    @Positive
    private long id;

    @NotNull
    @Positive
    private float pm25;
    @NotNull
    @Positive
    private float pm10;

}
