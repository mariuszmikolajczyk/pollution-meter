package io.gitlab.mmikolajczyk.pollutionmeter.queuecomponent.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import io.gitlab.mmikolajczyk.pollutionmeter.queuecomponent.config.RabbitConfig;
import io.gitlab.mmikolajczyk.pollutionmeter.queuecomponent.model.MeasurementDTO;
import io.gitlab.mmikolajczyk.pollutionmeter.queuecomponent.util.JSONMapper;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Service;

@Service
public class RabbitService {

    private RabbitTemplate rabbitTemplate;
    private RabbitConfig rabbitConfig;
    private JSONMapper jsonMapper;

    public RabbitService(RabbitTemplate rabbitTemplate, RabbitConfig rabbitConfig, JSONMapper jsonMapper) {
        this.rabbitTemplate = rabbitTemplate;
        this.rabbitConfig = rabbitConfig;
        this.jsonMapper = jsonMapper;
    }

    public void send(MeasurementDTO message) throws JsonProcessingException {
        String measurementJsonString = jsonMapper.mapToJSONString(message);
        rabbitTemplate.convertAndSend(rabbitConfig.getExchangeName(), rabbitConfig.getRoutingKey(), measurementJsonString);
    }

}
