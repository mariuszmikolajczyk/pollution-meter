package io.gitlab.mmikolajczyk.pollutionmeter.queuecomponent.config;

import lombok.Getter;
import org.springframework.amqp.core.AmqpAdmin;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;

//@Component
public class RabbitConfig implements CommandLineRunner {

    @Value("${rabbitmq.queue-name}")
    private String queueName;
    @Value("${rabbitmq.exchange-name}")
    @Getter
    private String exchangeName;

    @Value("${rabbitmq.queue-routing-key}")
    @Getter
    private String routingKey;

    private AmqpAdmin rabbitAdmin;

    public RabbitConfig(AmqpAdmin rabbitAdmin) {
        this.rabbitAdmin = rabbitAdmin;

    }

    @Override
    public void run(String... args) throws Exception {
        rabbitAdmin.declareExchange(new DirectExchange(exchangeName, false, false));
        rabbitAdmin.declareQueue(new Queue(queueName, false, false, false));
        rabbitAdmin.declareBinding(new Binding(queueName, Binding.DestinationType.QUEUE, exchangeName, routingKey, null));
    }
}
