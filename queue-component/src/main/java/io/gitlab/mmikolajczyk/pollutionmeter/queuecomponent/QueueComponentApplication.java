package io.gitlab.mmikolajczyk.pollutionmeter.queuecomponent;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class QueueComponentApplication {

	public static void main(String[] args) {
		SpringApplication.run(QueueComponentApplication.class, args);
	}

}
