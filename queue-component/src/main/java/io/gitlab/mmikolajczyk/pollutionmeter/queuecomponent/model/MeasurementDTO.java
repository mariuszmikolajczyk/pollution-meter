package io.gitlab.mmikolajczyk.pollutionmeter.queuecomponent.model;

import lombok.Data;

@Data
public class MeasurementDTO {

    private long id;
    private float pm25;
    private float pm10;
}
