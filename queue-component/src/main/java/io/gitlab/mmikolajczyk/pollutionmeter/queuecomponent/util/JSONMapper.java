package io.gitlab.mmikolajczyk.pollutionmeter.queuecomponent.util;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Component;

@Component
public class JSONMapper {

    private ObjectMapper objectMapper = new ObjectMapper();

    public JSONMapper(){
        objectMapper.setSerializationInclusion(JsonInclude.Include.USE_DEFAULTS);
    }

    public String mapToJSONString(Object object) throws JsonProcessingException {
        return objectMapper.writeValueAsString(object);
    }
}
