package io.gitlab.mmikolajczyk.pollutionmeter.queuecomponent.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import io.gitlab.mmikolajczyk.pollutionmeter.queuecomponent.controller.body.MeasurementBody;
import io.gitlab.mmikolajczyk.pollutionmeter.queuecomponent.model.MeasurementDTO;
import io.gitlab.mmikolajczyk.pollutionmeter.queuecomponent.model.mapper.MeasurementMapper;
import io.gitlab.mmikolajczyk.pollutionmeter.queuecomponent.service.RabbitService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/measurement/queue")
public class QueueController {

    private RabbitService rabbitService;

    public QueueController(RabbitService rabbitService) {
        this.rabbitService = rabbitService;
    }

    @PostMapping
    public void queueMeasurement(@RequestBody @Valid MeasurementBody measurementBody) throws JsonProcessingException {
        MeasurementDTO measureDto = MeasurementMapper.mapFromRequestBody(measurementBody);
        rabbitService.send(measureDto);
    }
}
