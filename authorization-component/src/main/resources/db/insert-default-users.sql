
insert into authorized_user(login, password, name) values('mario','$2a$10$WpbEOpd092s03ftUhRPDBObx05hvxYImqg0z6CornVj7Lb8JUGOFS','Mario');
insert into authorized_user(login, password, name) values('simulator','$2a$10$ejFHEi//BMeLBN/j9KrFaOC5C5WTDiY57fQsYu/uB/imHWnish8oK','Python simulator');
insert into authorized_user(login, password, name) values('web-console','$2a$10$ox22pxLg/e32F60A4qXOM.Mf8epfidtlsvhB53keKcRr1MqDhP4Qi','Web console application');


insert into user_user_role(role_id, user_id) values((select role_id from user_role where role_name='ROLE_DATA'),(select user_id from authorized_user where login='mario'));
insert into user_user_role(role_id, user_id) values((select role_id from user_role where role_name='ROLE_REGISTRATION'),(select user_id from authorized_user where login='mario'));
insert into user_user_role(role_id, user_id) values((select role_id from user_role where role_name='ROLE_REGISTRATION'),(select user_id from authorized_user where login='simulator'));
insert into user_user_role(role_id, user_id) values((select role_id from user_role where role_name='ROLE_DATA'),(select user_id from authorized_user where login='simulator'));
insert into user_user_role(role_id, user_id) values((select role_id from user_role where role_name='ROLE_DATA'),(select user_id from authorized_user where login='web-console'));
