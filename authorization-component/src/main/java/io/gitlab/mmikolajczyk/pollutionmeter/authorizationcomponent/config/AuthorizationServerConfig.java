package io.gitlab.mmikolajczyk.pollutionmeter.authorizationcomponent.config;

import io.gitlab.mmikolajczyk.pollutionmeter.authorizationcomponent.config.jwt.JwtClamsEnhancer;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.oauth2.authserver.AuthorizationServerProperties;
import org.springframework.boot.autoconfigure.security.oauth2.authserver.OAuth2AuthorizationServerConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.client.BaseClientDetails;
import org.springframework.security.oauth2.provider.token.AccessTokenConverter;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;
import org.springframework.security.oauth2.provider.token.TokenEnhancerChain;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;

import java.util.Collections;
import java.util.List;

/**
 * OAuth2AuthorizationServerConfiguration is provided by autoconfigure module, which setup basic configuration
 */
@Configuration
@EnableAuthorizationServer
public class AuthorizationServerConfig extends OAuth2AuthorizationServerConfiguration {

    private final AuthenticationManager authenticationManager;
    private final TokenStore tokenStore;
    private final JwtAccessTokenConverter jwtAccessTokenConverter;
    private final JwtClamsEnhancer jwtClamsEnhancer;

    @Autowired
    public AuthorizationServerConfig(AuthenticationConfiguration authenticationConfiguration, TokenStore tokenStore, JwtAccessTokenConverter jwtAccessTokenConverter, BaseClientDetails details, ObjectProvider<TokenStore> tokenStorePr, ObjectProvider<AccessTokenConverter> tokenConverter, AuthorizationServerProperties properties, JwtClamsEnhancer jwtClamsEnhancer) throws Exception {
        super(details, authenticationConfiguration, tokenStorePr, tokenConverter, properties);

        this.authenticationManager = authenticationConfiguration.getAuthenticationManager();
        this.tokenStore = tokenStore;
        this.jwtAccessTokenConverter = jwtAccessTokenConverter;
        this.jwtClamsEnhancer = jwtClamsEnhancer;
    }

    @Override
    public void configure(AuthorizationServerEndpointsConfigurer endpoints) {
        final var tokenEnhancerChain = new TokenEnhancerChain();
        tokenEnhancerChain.setTokenEnhancers(List.of(jwtClamsEnhancer, jwtAccessTokenConverter, new CleanAdditionalClaim()));
        endpoints
                .authenticationManager(authenticationManager)
                .tokenStore(tokenStore)
                .tokenEnhancer(tokenEnhancerChain);
    }

    /**
     * Removes additional claims adds in earlier enhancer
     * They are already included in access_token JWT value
     */
    static class CleanAdditionalClaim implements TokenEnhancer {
        @Override
        public OAuth2AccessToken enhance(OAuth2AccessToken accessToken, OAuth2Authentication authentication) {
            ((DefaultOAuth2AccessToken) accessToken).setAdditionalInformation(Collections.emptyMap());
            return accessToken;
        }
    }
}
