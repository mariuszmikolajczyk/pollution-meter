package io.gitlab.mmikolajczyk.pollutionmeter.authorizationcomponent;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;

@SpringBootApplication
@ConfigurationPropertiesScan
public class AuthorizationComponentApplication {

    public static void main(String[] args) {
        SpringApplication.run(AuthorizationComponentApplication.class, args);
    }

}
