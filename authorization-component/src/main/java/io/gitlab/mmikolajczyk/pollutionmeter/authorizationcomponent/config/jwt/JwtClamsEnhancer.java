package io.gitlab.mmikolajczyk.pollutionmeter.authorizationcomponent.config.jwt;

import io.gitlab.mmikolajczyk.pollutionmeter.authorizationcomponent.user.JdbcUserDetails;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.jwt.JwtClaimNames;
import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.time.Instant;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Component
public class JwtClamsEnhancer implements TokenEnhancer {
    private final ClientDetailsService clientDetailsService;

    public JwtClamsEnhancer(ClientDetailsService clientDetailsService) {
        this.clientDetailsService = clientDetailsService;
    }

    @Override
    public OAuth2AccessToken enhance(OAuth2AccessToken accessToken, OAuth2Authentication authentication) {
        final String issuer = ServletUriComponentsBuilder.fromCurrentRequest().build().toString();
        final Map<String, Object> additionalInformation = new LinkedHashMap<>();
        final Instant expiration = accessToken.getExpiration().toInstant();

        final Authentication client = SecurityContextHolder.getContext().getAuthentication();
        final String clientId = client.getName();
        final ClientDetails clientDetails = this.clientDetailsService.loadClientByClientId(clientId);

        additionalInformation.put(JwtClaimNames.ISS, issuer);
        additionalInformation.put(JwtClaimNames.EXP, expiration.getEpochSecond());
        additionalInformation.put(JwtClaimNames.IAT, expiration.minusSeconds(clientDetails.getAccessTokenValiditySeconds()).getEpochSecond());
        additionalInformation.put(JwtClaimNames.AUD, List.of(clientId));
        if (authentication.getPrincipal() instanceof JdbcUserDetails) {
            additionalInformation.put(JwtClaimNames.SUB, ((JdbcUserDetails) authentication.getPrincipal()).getName() /* UserID */);
        }
        ((DefaultOAuth2AccessToken) accessToken).setAdditionalInformation(additionalInformation);
        return accessToken;
    }
}
