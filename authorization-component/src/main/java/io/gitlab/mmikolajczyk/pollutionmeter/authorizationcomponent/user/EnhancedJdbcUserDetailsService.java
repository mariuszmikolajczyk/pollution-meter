package io.gitlab.mmikolajczyk.pollutionmeter.authorizationcomponent.user;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;

public class EnhancedJdbcUserDetailsService implements UserDetailsService {
    private final UserDetailsService delegateService;
    private final JdbcTemplate jdbcTemplate;

    public EnhancedJdbcUserDetailsService(UserDetailsService delegateService, JdbcTemplate jdbcTemplate) {
        this.delegateService = delegateService;
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public UserDetails loadUserByUsername(String username) {
        final var basicUserDetails = delegateService.loadUserByUsername(username);
        final String fullName = jdbcTemplate.queryForObject("select name from authorized_user where login=?", new Object[]{username}, String.class);

        return new JdbcUserDetails(basicUserDetails.getUsername(), basicUserDetails.getPassword(), basicUserDetails.getAuthorities(), fullName);
    }
}
