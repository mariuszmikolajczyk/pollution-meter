package io.gitlab.mmikolajczyk.pollutionmeter.authorizationcomponent.wellknown;

import com.nimbusds.jose.jwk.JWKSet;
import com.nimbusds.jose.jwk.RSAKey;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.KeyPair;
import java.security.interfaces.RSAPublicKey;
import java.util.Map;

@RestController
public class WellKnownEndpoint {
    private final KeyPair keyPair;

    public WellKnownEndpoint(KeyPair keyPair) {
        this.keyPair = keyPair;
    }

    @GetMapping("jwks.json")
    public Map<String, Object> jwksSet() {
        final var publicKey = (RSAPublicKey) keyPair.getPublic();
        final var rsaKey = new RSAKey.Builder(publicKey).build();
        return new JWKSet(rsaKey).toJSONObject();
    }
}
