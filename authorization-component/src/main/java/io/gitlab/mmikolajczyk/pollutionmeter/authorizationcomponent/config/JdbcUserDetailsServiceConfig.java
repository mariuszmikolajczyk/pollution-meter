package io.gitlab.mmikolajczyk.pollutionmeter.authorizationcomponent.config;

import io.gitlab.mmikolajczyk.pollutionmeter.authorizationcomponent.user.EnhancedJdbcUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.config.annotation.authentication.configurers.provisioning.JdbcUserDetailsManagerConfigurer;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import javax.sql.DataSource;
import java.security.SecureRandom;

@Configuration
public class JdbcUserDetailsServiceConfig {

    private DataSource dataSource;

    @Autowired
    public JdbcUserDetailsServiceConfig(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Bean
    public UserDetailsService userDetailsServiceBean(JdbcTemplate jdbcTemplate) {
        final UserDetailsService basicService = new JdbcUserDetailsManagerConfigurer<>()
                .dataSource(dataSource)
                .authoritiesByUsernameQuery("select login, role_name from authorized_user u join user_user_role uur on u.user_id = uur.user_id join user_role ur on uur.role_id = ur.role_id where login=?")
                .usersByUsernameQuery("select login,password,enabled from authorized_user where login=?")
                .getUserDetailsService();
        return new EnhancedJdbcUserDetailsService(basicService, jdbcTemplate);
    }

    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder(10, new SecureRandom());
    }
}
